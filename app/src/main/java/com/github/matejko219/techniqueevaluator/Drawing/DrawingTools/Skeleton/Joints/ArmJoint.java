/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Skeleton.Joints;

import android.os.Parcel;
import android.os.Parcelable;

import com.github.matejko219.techniqueevaluator.Drawing.Point;
import com.github.matejko219.techniqueevaluator.Evaluation.EvaluationUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DELL on 2017-09-22.
 */
public class ArmJoint implements PointList, Parcelable, Serializable {

    private Point shoulder;
    private Point elbow;
    private Point wrist;

    public ArmJoint() {
    }

    protected ArmJoint(Parcel in) {
        shoulder = in.readParcelable(Point.class.getClassLoader());
        elbow = in.readParcelable(Point.class.getClassLoader());
        wrist = in.readParcelable(Point.class.getClassLoader());
    }

    public Point getShoulder() {
        return shoulder;
    }

    public void setShoulder(Point shoulder) {
        this.shoulder = shoulder;
    }

    public Point getElbow() {
        return elbow;
    }

    public void setElbow(Point elbow) {
        this.elbow = elbow;
    }

    public Point getWrist() {
        return wrist;
    }

    public void setWrist(Point wrist) {
        this.wrist = wrist;
    }

    public float getInnerElbowAngle() {
        float angle = EvaluationUtils.angleBetween2Lines(wrist, elbow, shoulder);
        return angle <= 180 ? angle : EvaluationUtils.angleBetween2Lines(shoulder, elbow, wrist);
    }

    @Override
    public List<Point> getPointList() {
        List<Point> result = new ArrayList<>();
        if (shoulder != null)result.add(shoulder);
        if (elbow != null)result.add(elbow);
        if (wrist != null)result.add(wrist);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(shoulder, flags);
        dest.writeParcelable(elbow, flags);
        dest.writeParcelable(wrist, flags);
    }

    public static final Creator<ArmJoint> CREATOR = new Creator<ArmJoint>() {
        @Override
        public ArmJoint createFromParcel(Parcel in) {
            return new ArmJoint(in);
        }

        @Override
        public ArmJoint[] newArray(int size) {
            return new ArmJoint[size];
        }
    };
}
