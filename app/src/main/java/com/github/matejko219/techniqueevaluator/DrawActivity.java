/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import com.github.matejko219.techniqueevaluator.Drawing.DrawingView;
import com.github.matejko219.techniqueevaluator.Drawing.DrawnPointsCoordinates;
import com.github.matejko219.techniqueevaluator.Helpers.DialogHelper;
import com.github.matejko219.techniqueevaluator.Helpers.FileHelper;
import com.github.matejko219.techniqueevaluator.Helpers.ImageLoadingHelper;

import java.io.IOException;

public class DrawActivity extends AppCompatActivity {

    DrawingView drawingView;
    NavigationView navigationView;
    DrawerLayout drawerLayout;
    public static final String ACT_RESULT_DATA = "DRAW_RESULT";
    DrawingView.Mode mode = DrawingView.Mode.ADD;
    boolean isDataSet = false;
    public static final String SAVED_MODE = "MODE";
    public static final String SAVED_IS_DATA_SET = "IS_DATA_SET";
    private Context context;
    private MenuItem addModeMenuItem;
    private MenuItem editModeMenuItem;
    private MenuItem deleteModeMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw);
        context = this;

        restoreState(savedInstanceState);

        setImmersiveMode();
        prepareNavigationView();
        prepareDrawingView();
    }

    private void prepareNavigationView() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.drawing_nav);

        addModeMenuItem = navigationView.getMenu().findItem(R.id.action_draw_add);
        editModeMenuItem = navigationView.getMenu().findItem(R.id.action_draw_edit);
        deleteModeMenuItem = navigationView.getMenu().findItem(R.id.action_draw_delete);

        navigationView.setCheckedItem(R.id.action_draw_skeleton);

        setMenuItemsCheck();

        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.action_draw_add:
                            mode = DrawingView.Mode.ADD;
                            drawingView.setMode(mode);
                            setMenuItemsCheck();
                            break;

                        case R.id.action_draw_edit:
                            mode = DrawingView.Mode.EDIT;
                            drawingView.setMode(mode);
                            setMenuItemsCheck();
                            break;

                        case R.id.action_draw_delete:
                            mode = DrawingView.Mode.DELETE;
                            drawingView.setMode(mode);
                            setMenuItemsCheck();
                            break;

                        case R.id.action_draw_skeleton:
                            drawingView.setTool(DrawingView.Tool.SKELETON);
                            navigationView.setCheckedItem(item.getItemId());
                            break;

                        case R.id.action_draw_ruler:
                            drawingView.setTool(DrawingView.Tool.RULER);
                            navigationView.setCheckedItem(item.getItemId());
                            break;

                        case R.id.action_draw_protractor:
                            drawingView.setTool(DrawingView.Tool.PROTRACTOR);
                            navigationView.setCheckedItem(item.getItemId());
                            break;

                        case R.id.action_draw_confirm:
                            onBackPressed();
                            break;

                        case R.id.action_draw_info:
                            Intent infoIntent = new Intent(context, InfoActivity.class);
                            startActivity(infoIntent);
                            break;

                        case R.id.action_draw_save_image:
                            saveDrawingViewImage(drawingView.getDrawingCache());
                            break;

                        case R.id.action_draw_results:
                            showResults();
                            break;

                        case R.id.action_draw_reset:
                            resetDrawingView();
                            break;
                    }

                    drawerLayout.closeDrawers();
                    return false;
                }
            });
        }
    }

    private void saveDrawingViewImage(Bitmap drawingCache) {
        if (FileHelper.saveBitmap(drawingCache, FileHelper.AppDirectory.SAVED_IMAGES, DrawActivity.this, null))
            DialogHelper.showToast(DrawActivity.this, getString(R.string.image_saved));
        else
            DialogHelper.showToast(DrawActivity.this, getString(R.string.image_save_error));
    }

    private void setMenuItemsCheck() {
        addModeMenuItem.setChecked(mode == DrawingView.Mode.ADD);
        editModeMenuItem.setChecked(mode == DrawingView.Mode.EDIT);
        deleteModeMenuItem.setChecked(mode == DrawingView.Mode.DELETE);
    }

    private void showResults() {
        DialogHelper.showResults(context, drawingView.getDrawnPointsCoordinates());
    }

    private void resetDrawingView() {
        DialogHelper.showConfirm(context, getString(R.string.confirm_reset), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                drawingView.resetView();
            }
        });
    }

    private void prepareDrawingView() {
        drawingView = (DrawingView) findViewById(R.id.drawing_view);
        drawingView.setActivityContext(this);
        drawingView.setDrawingCacheEnabled(true);

        drawingView.setMode(mode);
        if (!isDataSet) setDataIntoDrawingView();
    }

    private void setDataIntoDrawingView() {
        try {
            String uriExtra = getIntent().getStringExtra(ImageSelectActivity.EXTRA_FRAME_URI);
            Uri bitmapUri = Uri.parse(uriExtra);
            Bitmap bitmap = ImageLoadingHelper.getBitmapFromUri(this, bitmapUri);
            drawingView.setSelectedBitmap(bitmap);

            DrawnPointsCoordinates dpc = getIntent().getParcelableExtra(ImageSelectActivity.EXTRA_DPC);
            drawingView.setDrawnPointsCoordinates(dpc);
            isDataSet = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        Bundle resultBundle = new Bundle();
        resultBundle.putParcelable(ACT_RESULT_DATA, drawingView.getDrawnPointsCoordinates());
        Intent resultIntent = new Intent();
        resultIntent.putExtras(resultBundle);
        setResult(RESULT_OK, resultIntent);
        super.onBackPressed();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) setImmersiveMode();
    }

    private void setImmersiveMode() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(SAVED_MODE, mode);
        outState.putBoolean(SAVED_IS_DATA_SET, isDataSet);
        super.onSaveInstanceState(outState);
    }

    private void restoreState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mode = (DrawingView.Mode)savedInstanceState.getSerializable(SAVED_MODE);
            isDataSet = savedInstanceState.getBoolean(SAVED_IS_DATA_SET);
        }
    }

}