/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Drawing;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by DELL on 2018-02-07.
 */
public class FrameItem implements Parcelable, Serializable {

    private transient Bitmap thumbnail;
    private transient Uri uri;
    private DrawnPointsCoordinates drawnPointsCoordinates;

    public FrameItem(Bitmap thumbnail, Uri uri) {
        this.thumbnail = thumbnail;
        this.uri = uri;
        this.drawnPointsCoordinates = new DrawnPointsCoordinates();
    }

    protected FrameItem(Parcel in) {
        thumbnail = in.readParcelable(Bitmap.class.getClassLoader());
        uri = Uri.parse(in.readString());
        drawnPointsCoordinates = in.readParcelable(DrawnPointsCoordinates.class.getClassLoader());
    }

    public Bitmap getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Bitmap thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public DrawnPointsCoordinates getDrawnPointsCoordinates() {
        return drawnPointsCoordinates;
    }

    public void setDrawnPointsCoordinates(DrawnPointsCoordinates drawnPointsCoordinates) {
        this.drawnPointsCoordinates = drawnPointsCoordinates;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(thumbnail, flags);
        dest.writeString(uri.toString());
        dest.writeParcelable(drawnPointsCoordinates, flags);
    }

    public static final Creator<FrameItem> CREATOR = new Creator<FrameItem>() {
        @Override
        public FrameItem createFromParcel(Parcel in) {
            return new FrameItem(in);
        }

        @Override
        public FrameItem[] newArray(int size) {
            return new FrameItem[size];
        }
    };
}
