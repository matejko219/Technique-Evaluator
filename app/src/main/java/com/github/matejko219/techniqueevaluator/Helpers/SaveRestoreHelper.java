/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Helpers;

import android.app.Activity;
import android.content.ContentResolver;
import android.net.Uri;
import android.provider.MediaStore;

import com.github.matejko219.techniqueevaluator.Drawing.FrameItem;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mateusz Tumidajewicz (matejko219) on 2018-03-25.
 */
public class SaveRestoreHelper {

    public static void deletePreviousFileBeforeSave(Activity activity, File file) {
        deleteFileFromMediaStore(activity.getContentResolver(), file);
    }

    public static File createSaveDirectory(String projectName) {
        File saveDirectory = getSaveDirectory(projectName);
        saveDirectory.mkdirs();
        return saveDirectory;
    }

    public static File getProjectSaveFile(String projectName) {
        return new File(getSaveDirectory(projectName), projectFileNamePattern(projectName));
    }

    public static String projectFileNamePattern(String projectName) {
        return String.format("%s.tepf", projectName);
    }

    public static File getProjectBitmapSaveFile(String projectName, int index) {
        return new File(getSaveDirectory(projectName), imageFileNamePattern(index));
    }

    public static String imageFileNamePattern(int index) {
        return String.format("%02d.png", index);
    }

    private static File getSaveDirectory(String projectName) {
        return new File(FileHelper.getAppDirectory(FileHelper.AppDirectory.SAVED_PROJECTS), projectName);
    }

    public static boolean checkIfExist(String projectName) {
        return getSaveDirectory(projectName).exists();
    }

    public static void deleteObsoleteFilesInSaveDirectory(String projectName, File directory, List<FrameItem> frameItems, Activity activity) {
        if (directory.isDirectory()) {

            List<String> usedImageNames = new ArrayList<>();
            for (int i = 0; i < frameItems.size(); i++) {
                usedImageNames.add(imageFileNamePattern(i));
            }

            String projectFileName = projectFileNamePattern(projectName);

            for (File child : directory.listFiles()) {
                String childFileName = child.getName();
                if (!usedImageNames.contains(childFileName) && !childFileName.equals(projectFileName))
                    deleteFileFromMediaStore(activity.getContentResolver(), child);
            }
        }
    }

    public static boolean deleteProjectDirectory(ContentResolver contentResolver, String projectName) {
        File saveDirectory = getSaveDirectory(projectName);
        boolean result = true;

        if (saveDirectory.isDirectory()) {
            for (File child : saveDirectory.listFiles()) {
                if (child.getName().equals(projectFileNamePattern(projectName)))
                    result = result && child.delete();
                else result = result && deleteFileFromMediaStore(contentResolver, child);
            }
        }

        return result && saveDirectory.delete();
    }

    private static boolean deleteFileFromMediaStore(final ContentResolver contentResolver, final File file) {
        String canonicalPath;
        try {
            canonicalPath = file.getCanonicalPath();
        } catch (IOException e) {
            canonicalPath = file.getAbsolutePath();
        }

        final Uri uri = MediaStore.Files.getContentUri("external");
        int result = contentResolver.delete(uri,
                MediaStore.Files.FileColumns.DATA + "=?", new String[] {canonicalPath});

        if (result == 0) {
            final String absolutePath = file.getAbsolutePath();
            if (!absolutePath.equals(canonicalPath)) {
                result = contentResolver.delete(uri,
                        MediaStore.Files.FileColumns.DATA + "=?", new String[]{absolutePath});
            }
        }

        return result != 0;
    }

}
