/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Tasks;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;

import com.github.matejko219.techniqueevaluator.Drawing.FrameItem;
import com.github.matejko219.techniqueevaluator.Helpers.FileHelper;
import com.github.matejko219.techniqueevaluator.Helpers.ImageLoadingHelper;
import com.github.matejko219.techniqueevaluator.Helpers.SaveRestoreHelper;
import com.github.matejko219.techniqueevaluator.Helpers.ScanFileCallback;
import com.github.matejko219.techniqueevaluator.Tasks.Exceptions.SaveProjectException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Mateusz Tumidajewicz (matejko219) on 2018-03-29.
 */
public class SaveProjectAsyncTask extends AsyncTask<List<FrameItem>, Integer, Void> {

    private Activity activity;
    private String projectName;
    private SaveProjectAsyncCallbackHandler callbackHandler;

    public SaveProjectAsyncTask(Activity activity, String projectName, SaveProjectAsyncCallbackHandler callbackHandler) {
        this.activity = activity;
        this.projectName = projectName;
        this.callbackHandler = callbackHandler;
    }

    @Override
    protected Void doInBackground(List<FrameItem>... params) {
        final List<FrameItem> frameItems = params[0];
        final File saveDirectory = SaveRestoreHelper.createSaveDirectory(projectName);
        File projectSaveFile = SaveRestoreHelper.getProjectSaveFile(projectName);

        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        try {
            fos = new FileOutputStream(projectSaveFile);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(frameItems);
            FileHelper.scanFile(projectSaveFile, activity, null);

        } catch (IOException e) {
            e.printStackTrace();
            callbackHandler.onException(new SaveProjectException(activity, SaveRestoreHelper.projectFileNamePattern(projectName), e));
            return null;

        } finally {
            try {
                if (oos != null) oos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            
            try {
                if (fos != null) fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        final AtomicInteger executeCounter = new AtomicInteger();

        int index = 0;
        for (FrameItem frameItem : frameItems) {
            try {
                Bitmap bitmap = ImageLoadingHelper.getBitmapFromUri(activity, frameItem.getUri());
                File bitmapFileToSave = SaveRestoreHelper.getProjectBitmapSaveFile(projectName, index);
                SaveRestoreHelper.deletePreviousFileBeforeSave(activity, bitmapFileToSave);
                FileHelper.saveBitmapToFile(bitmap, bitmapFileToSave);
                FileHelper.scanFile(bitmapFileToSave, activity, new ScanFileCallback() {
                    @Override
                    public void onScanCompleted(Uri uri) {
                        if (executeCounter.incrementAndGet() == frameItems.size()) {
                            SaveRestoreHelper.deleteObsoleteFilesInSaveDirectory(projectName, saveDirectory, frameItems, activity);
                            callbackHandler.onSave();
                        }
                    }
                });
                index++;
            } catch (Exception e) {
                e.printStackTrace();
                callbackHandler.onException(new SaveProjectException(activity, SaveRestoreHelper.imageFileNamePattern(index), e));
            }
        }

        return null;
    }

    public interface SaveProjectAsyncCallbackHandler {
        void onSave();
        void onException(SaveProjectException e);
    }
}
