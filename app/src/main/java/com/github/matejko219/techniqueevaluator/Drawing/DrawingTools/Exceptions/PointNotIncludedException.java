/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Exceptions;

import com.github.matejko219.techniqueevaluator.Drawing.DrawingView;
import com.github.matejko219.techniqueevaluator.Drawing.Point;

/**
 * Created by DELL on 2017-10-31.
 */
public class PointNotIncludedException extends Exception {

    public PointNotIncludedException(DrawingView.Tool tool, Point point) {
        super(String.format("%s not included in selected %s", point.toString(), tool.name()));
    }

}
