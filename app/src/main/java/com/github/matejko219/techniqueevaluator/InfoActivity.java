/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

public class InfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        ImageView imageView = (ImageView) findViewById(R.id.info_image_view);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 4;
        Bitmap infoImage = BitmapFactory.decodeResource(getResources(), R.drawable.info_image, options);
        imageView.setImageBitmap(infoImage);
    }
}
