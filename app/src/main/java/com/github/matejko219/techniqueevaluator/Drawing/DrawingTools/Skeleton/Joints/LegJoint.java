/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Skeleton.Joints;

import android.os.Parcel;
import android.os.Parcelable;

import com.github.matejko219.techniqueevaluator.Drawing.Point;
import com.github.matejko219.techniqueevaluator.Evaluation.EvaluationUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DELL on 2017-09-22.
 */
public class LegJoint implements PointList, Parcelable, Serializable {

    private Point hip;
    private Point knee;
    private Point ankle;
    private Point foot;

    public LegJoint() {
    }

    protected LegJoint(Parcel in) {
        hip = in.readParcelable(Point.class.getClassLoader());
        knee = in.readParcelable(Point.class.getClassLoader());
        ankle = in.readParcelable(Point.class.getClassLoader());
        foot = in.readParcelable(Point.class.getClassLoader());
    }

    public Point getHip() {
        return hip;
    }

    public void setHip(Point hip) {
        this.hip = hip;
    }

    public Point getKnee() {
        return knee;
    }

    public void setKnee(Point knee) {
        this.knee = knee;
    }

    public Point getAnkle() {
        return ankle;
    }

    public void setAnkle(Point ankle) {
        this.ankle = ankle;
    }

    public Point getFoot() {
        return foot;
    }

    public void setFoot(Point foot) {
        this.foot = foot;
    }

    public float getInnerKneeAngle() {
        float angle = EvaluationUtils.angleBetween2Lines(hip, knee, ankle);
        return angle <= 180 ? angle : EvaluationUtils.angleBetween2Lines(ankle, knee, hip);
    }

    @Override
    public List<Point> getPointList() {
        List<Point> result = new ArrayList<>();
        if (hip != null)result.add(hip);
        if (knee != null)result.add(knee);
        if (ankle != null)result.add(ankle);
        if (foot != null)result.add(foot);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(hip, flags);
        dest.writeParcelable(knee, flags);
        dest.writeParcelable(ankle, flags);
        dest.writeParcelable(foot, flags);
    }

    public static final Creator<LegJoint> CREATOR = new Creator<LegJoint>() {
        @Override
        public LegJoint createFromParcel(Parcel in) {
            return new LegJoint(in);
        }

        @Override
        public LegJoint[] newArray(int size) {
            return new LegJoint[size];
        }
    };
}
