/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Adapters;

import java.text.SimpleDateFormat;

/**
 * Created by Mateusz Tumidajewicz (matejko219) on 2018-03-29.
 */
public class ProjectFile {

    private String name;
    private String modifyDate;

    public ProjectFile(String name, long modifyDate) {
        this.name = name;
        this.setModifyDate(modifyDate);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(long modifyDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        this.modifyDate = sdf.format(modifyDate);
    }
}
