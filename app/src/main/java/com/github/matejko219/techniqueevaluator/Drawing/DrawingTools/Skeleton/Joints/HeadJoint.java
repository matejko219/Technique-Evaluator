/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Skeleton.Joints;

import android.os.Parcel;
import android.os.Parcelable;

import com.github.matejko219.techniqueevaluator.Drawing.Point;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DELL on 2017-09-22.
 */
public class HeadJoint implements PointList, Parcelable, Serializable {
    private Point head;
    private Point neck;

    public HeadJoint() {
    }

    protected HeadJoint(Parcel in) {
        head = in.readParcelable(Point.class.getClassLoader());
        neck = in.readParcelable(Point.class.getClassLoader());
    }

    public Point getHead() {
        return head;
    }

    public void setHead(Point head) {
        this.head = head;
    }

    public Point getNeck() {
        return neck;
    }

    public void setNeck(Point neck) {
        this.neck = neck;
    }

    public void setNeckFromShoulders(Point leftShoulder, Point rightShoulder) {
        float middleX = ((leftShoulder.x + rightShoulder.x) / 2f);
        float middleY = ((leftShoulder.y + rightShoulder.y) / 2f);
        setNeck(new Point(middleX, middleY));
    }

    @Override
    public List<Point> getPointList() {
        List<Point> result = new ArrayList<>();
        if (head != null)result.add(head);
        if (neck != null)result.add(neck);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(head, flags);
        dest.writeParcelable(neck, flags);
    }

    public static final Creator<HeadJoint> CREATOR = new Creator<HeadJoint>() {
        @Override
        public HeadJoint createFromParcel(Parcel in) {
            return new HeadJoint(in);
        }

        @Override
        public HeadJoint[] newArray(int size) {
            return new HeadJoint[size];
        }
    };
}
