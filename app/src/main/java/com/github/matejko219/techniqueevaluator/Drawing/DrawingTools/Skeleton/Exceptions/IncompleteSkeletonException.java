/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Skeleton.Exceptions;

/**
 * Created by DELL on 2017-12-10.
 */
public class IncompleteSkeletonException extends Exception {

    public IncompleteSkeletonException(int actualSize) {
        super(String.format("Already defined %d points", actualSize));
    }

}
