/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        TextView versionTv = (TextView)findViewById(R.id.version_tv);
        versionTv.setText("v" + BuildConfig.VERSION_NAME);
    }

    public void onLinkClick(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW);

        String url = "";
        switch (view.getId()) {
            case R.id.my_github:
                url = "https://github.com/matejko219";
                break;
            case R.id.spectaculum_github:
                url = getString(R.string.spectaculumUrl);
                break;
        }

        intent.setData(Uri.parse(url));
        startActivity(intent);
    }
}
