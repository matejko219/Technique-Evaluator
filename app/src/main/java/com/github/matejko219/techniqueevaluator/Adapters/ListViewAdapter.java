/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.github.matejko219.techniqueevaluator.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mateusz Tumidajewicz (matejko219) on 2018-03-29.
 */
public class ListViewAdapter extends ArrayAdapter {

    private Context context;
    private int resource;
    private List<ProjectFile> data = new ArrayList<>();

    public ListViewAdapter(Context context, int resource, List objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.data = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(resource, parent, false);
            holder = new ViewHolder();
            holder.fileName = (TextView)row.findViewById(R.id.file_name);
            holder.modDate = (TextView)row.findViewById(R.id.mod_date);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        ProjectFile projectFile = data.get(position);
        holder.fileName.setText(projectFile.getName());
        holder.modDate.setText(projectFile.getModifyDate());

        return row;
    }

    static class ViewHolder {
        TextView fileName;
        TextView modDate;
    }
}
