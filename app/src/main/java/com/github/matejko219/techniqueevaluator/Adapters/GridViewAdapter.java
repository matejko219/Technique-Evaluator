/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import com.github.matejko219.techniqueevaluator.Drawing.FrameItem;
import com.github.matejko219.techniqueevaluator.R;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DELL on 2018-02-07.
 */
public class GridViewAdapter extends ArrayAdapter {

    private Context context;
    private int resource;
    private List<FrameItem> data = new ArrayList<>();

    public GridViewAdapter(Context context, int resource, List objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.data = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(resource, parent, false);
            holder = new ViewHolder();
            holder.imageView = (ImageView)row.findViewById(R.id.image);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        FrameItem frameItem = data.get(position);
        holder.imageView.setImageBitmap(frameItem.getThumbnail());
        return row;
    }

    static class ViewHolder {
        ImageView imageView;
    }
}
