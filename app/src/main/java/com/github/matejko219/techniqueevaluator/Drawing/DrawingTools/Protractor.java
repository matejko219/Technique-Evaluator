/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Drawing.DrawingTools;

import android.os.Parcel;
import android.os.Parcelable;

import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Exceptions.TooManyPointsException;
import com.github.matejko219.techniqueevaluator.Drawing.DrawingView;
import com.github.matejko219.techniqueevaluator.Drawing.Point;
import com.github.matejko219.techniqueevaluator.Evaluation.EvaluationUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mateusz Tumidajewicz (matejko219) on 2018-02-25.
 */
public class Protractor extends DrawingTool implements Parcelable, Serializable {

    private Point first;
    private Point fixed;
    private Point second;

    public Protractor() {
        super(3, DrawingView.Tool.PROTRACTOR);
    }

    protected Protractor(Parcel in) {
        super(in, 3, DrawingView.Tool.PROTRACTOR);
        first = in.readParcelable(Point.class.getClassLoader());
        fixed = in.readParcelable(Point.class.getClassLoader());
        second = in.readParcelable(Point.class.getClassLoader());
    }

    public Point getFirst() {
        return first;
    }

    public Point getFixed() {
        return fixed;
    }

    public Point getSecond() {
        return second;
    }

    @Override
    public void addPoint(Point point) throws TooManyPointsException{
        if (!canAddAnotherPoint()) {
            throw new TooManyPointsException(super.tool, super.maxNumberOfPoints, super.numberOfPoints);
        }

        switch (numberOfPoints) {
            case 0:
                first = point;
                break;
            case 1:
                fixed = point;
                break;
            case 2:
                second = point;
                break;
            default:
                throw new TooManyPointsException(super.tool, super.maxNumberOfPoints, super.numberOfPoints);
        }

        numberOfPoints++;
    }

    @Override
    public List<Point> getAllEditablePoints() {
        List<Point> allEditablePoints = new ArrayList<>();

        if (first != null) allEditablePoints.add(first);
        if (fixed != null) allEditablePoints.add(fixed);
        if (second != null) allEditablePoints.add(second);

        return allEditablePoints;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(first, flags);
        dest.writeParcelable(fixed, flags);
        dest.writeParcelable(second, flags);
    }

    public static final Creator<Protractor> CREATOR = new Creator<Protractor>() {
        @Override
        public Protractor createFromParcel(Parcel in) {
            return new Protractor(in);
        }

        @Override
        public Protractor[] newArray(int size) {
            return new Protractor[size];
        }
    };

    public float getAngle() {
        return EvaluationUtils.angleBetween2Lines(first, fixed, second);
    }
}
