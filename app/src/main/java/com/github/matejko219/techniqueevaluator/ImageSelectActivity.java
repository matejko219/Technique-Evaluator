/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;

import com.github.matejko219.techniqueevaluator.Adapters.GridViewAdapter;
import com.github.matejko219.techniqueevaluator.Drawing.DrawnPointsCoordinates;
import com.github.matejko219.techniqueevaluator.Drawing.FrameItem;
import com.github.matejko219.techniqueevaluator.Helpers.DialogHelper;
import com.github.matejko219.techniqueevaluator.Helpers.FileHelper;
import com.github.matejko219.techniqueevaluator.Helpers.ImageLoadingHelper;
import com.github.matejko219.techniqueevaluator.Helpers.SaveRestoreHelper;
import com.github.matejko219.techniqueevaluator.Tasks.Exceptions.RestoreProjectException;
import com.github.matejko219.techniqueevaluator.Tasks.Exceptions.SaveProjectException;
import com.github.matejko219.techniqueevaluator.Tasks.RestoreProjectAsyncTask;
import com.github.matejko219.techniqueevaluator.Tasks.SaveProjectAsyncTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ImageSelectActivity extends AppCompatActivity {

    private GridViewAdapter gridViewAdapter;
    private FloatingActionButton fileSelectFab;
    private GridView gridView;
    private View progressWrapper;
    private static final int SELECT_PICTURE_REQ_CODE = 1;
    private static final int SELECT_VIDEO_REQ_CODE = 2;
    private static final int DRAW_ACT_REQ_CODE = 3;
    private static final int FRAME_CAPTURE_REQ_CODE = 4;
    private static final int LOAD_PROJECT_REQ_CODE = 5;
    private static final String SAVED_GRID_DATA = "SAVED_GRID_DATA";
    private static final String SAVED_FRAME_INDEX = "SAVED_FRAME_INDEX";
    private static final String SAVED_CURRENT_PROJ_NAME = "SAVED_CURRENT_PROJ_NAME";
    private ArrayList<FrameItem> gridViewAdapterData;
    private Context context;
    public static final String EXTRA_FRAME_URI = "EXTRA_FRAME_URI";
    public static final String EXTRA_DPC = "EXTRA_DPC";
    public static final String EXTRA_VIDEO_URI = "EXTRA_VIDEO_URI";
    private int selectedFrameIndex = -1;
    private boolean inProgress = false;
    private String currentProjectName = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_select);
        initActionBar();

        context = this;
        fileSelectFab = (FloatingActionButton) findViewById(R.id.file_select_fab);
        progressWrapper = findViewById(R.id.progress_wrapper);
        gridView = (GridView) findViewById(R.id.grid_view);
        gridViewAdapterData = new ArrayList<>();
        gridViewAdapter = new GridViewAdapter(this, R.layout.grid_item_layout, gridViewAdapterData);
        gridView.setAdapter(gridViewAdapter);


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (inProgress) return;
                selectedFrameIndex = position;
                FrameItem frameItem = (FrameItem) parent.getItemAtPosition(position);
                Intent drawingIntent = new Intent(context, DrawActivity.class);
                drawingIntent.putExtra(EXTRA_FRAME_URI, frameItem.getUri().toString());
                drawingIntent.putExtra(EXTRA_DPC, (Parcelable) frameItem.getDrawnPointsCoordinates());
                startActivityForResult(drawingIntent, DRAW_ACT_REQ_CODE);
            }
        });

        registerForContextMenu(gridView);
        registerForContextMenu(fileSelectFab);
    }

    @Override
    public void onBackPressed() {
        if (gridViewAdapterData.size() > 0)
            DialogHelper.showConfirm(ImageSelectActivity.this, getString(R.string.confirm_img_select_exit), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ImageSelectActivity.super.onBackPressed();
                }
            });
        else super.onBackPressed();
    }

    private void initActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.image_select_toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.image_select_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (inProgress) return false;
        int id = item.getItemId();

        switch (id) {
            case R.id.action_save_project:
                saveProject();
                break;
            case R.id.action_load_project:
                restoreProject();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveProject() {
        if (ImageLoadingHelper.isStoragePermissionGranted(ImageSelectActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) &&
                ImageLoadingHelper.isStoragePermissionGranted(ImageSelectActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            if (gridViewAdapterData.isEmpty())
                DialogHelper.showError(ImageSelectActivity.this, getString(R.string.empty_project_error));
            else {
                final EditText projectNameInput = new EditText(ImageSelectActivity.this);
                projectNameInput.setText(getProjectFileName());

                AlertDialog.Builder builder = new AlertDialog.Builder(ImageSelectActivity.this);
                builder
                        .setTitle(R.string.project_name_prompt)
                        .setView(projectNameInput)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                final String projectName = projectNameInput.getText().toString().trim();

                                if (!FileHelper.isFileNameValid(projectName)) {
                                    DialogHelper.showError(ImageSelectActivity.this, getString(R.string.bad_filename_error));
                                    return;
                                }

                                if (SaveRestoreHelper.checkIfExist(projectName))
                                    DialogHelper.showConfirm(ImageSelectActivity.this, String.format(getString(R.string.already_exist_error), projectName), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            saveProjectToFile(projectName);
                                        }
                                    });
                                else saveProjectToFile(projectName);
                            }
                        })
                        .show();
            }
        }
    }

    private String getProjectFileName() {
        return currentProjectName != null ? currentProjectName : FileHelper.getShortTimeStamp();
    }

    private void saveProjectToFile(final String projectName) {
        setProgressBar(true);

        SaveProjectAsyncTask saveProjectAsyncTask = new SaveProjectAsyncTask(ImageSelectActivity.this, projectName, new SaveProjectAsyncTask.SaveProjectAsyncCallbackHandler() {
            @Override
            public void onSave() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setProgressBar(false);
                        DialogHelper.showToast(ImageSelectActivity.this, getString(R.string.project_saved));
                        currentProjectName = projectName;
                        restoreProjectFromFile(currentProjectName);
                    }
                });
            }

            @Override
            public void onException(final SaveProjectException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setProgressBar(false);
                        DialogHelper.showToast(ImageSelectActivity.this, e.getMessage());
                    }
                });
            }
        });

        saveProjectAsyncTask.execute(gridViewAdapterData);
    }

    private void restoreProject() {
        if (ImageLoadingHelper.isStoragePermissionGranted(ImageSelectActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Intent loadProjectIntent = new Intent(ImageSelectActivity.this, ProjectListActivity.class);
            startActivityForResult(loadProjectIntent, LOAD_PROJECT_REQ_CODE);
        }
    }

    private void restoreProjectFromFile(final String projectName) {
        setProgressBar(true);

        RestoreProjectAsyncTask restoreProjectAsyncTask = new RestoreProjectAsyncTask(ImageSelectActivity.this, new RestoreProjectAsyncTask.RestoreProjectAsyncCallbackHandler() {
            @Override
            public void onRestore(final ArrayList<FrameItem> restoredFrames) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        gridViewAdapterData.clear();
                        gridViewAdapterData.addAll(restoredFrames);
                        gridViewAdapter.notifyDataSetChanged();
                        setProgressBar(false);
                        currentProjectName = projectName;
                    }
                });
            }

            @Override
            public void onException(final RestoreProjectException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setProgressBar(false);
                        DialogHelper.showToast(ImageSelectActivity.this, e.getMessage());
                    }
                });
            }
        });

        restoreProjectAsyncTask.execute(projectName);
    }

    private void setProgressBar(boolean active) {
        progressWrapper.setVisibility(active ? View.VISIBLE : View.GONE);
        inProgress = active;
        fileSelectFab.setClickable(!active);
        fileSelectFab.setLongClickable(!active);
        gridView.setClickable(!active);
        gridView.setLongClickable(!active);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater menuInflater = getMenuInflater();

        switch (v.getId()) {
            case R.id.grid_view:
                menuInflater.inflate(R.menu.grid_item_menu, menu);
                break;
            case R.id.file_select_fab:
                menuInflater.inflate(R.menu.add_menu, menu);
                break;
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()) {
            case R.id.action_grid_item_results:
                showResults(info.position);
                return true;
            case R.id.action_grid_item_remove:
                confirmGridItemRemove(info.position);
                return true;
            case R.id.action_add_image:
                choseImageFile();
                return true;
            case R.id.action_add_video:
                choseVideoFile();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    public void openFileContextMenu(View view) {
        openContextMenu(fileSelectFab);
    }

    private void showResults(int position) {
        FrameItem frameItem = gridViewAdapterData.get(position);
        DialogHelper.showResults(context, frameItem.getDrawnPointsCoordinates());
    }

    private void confirmGridItemRemove(final int position) {
        DialogHelper.showConfirm(context, getString(R.string.confrim_frame_remove), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                removeGridItem(position);
            }
        });
    }

    private void removeGridItem(int position) {
        gridViewAdapterData.remove(position);
        gridViewAdapter.notifyDataSetChanged();
    }

    public void choseImageFile() {
        if (ImageLoadingHelper.isStoragePermissionGranted(ImageSelectActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.select_image)), SELECT_PICTURE_REQ_CODE);
        }
    }

    public void choseVideoFile() {
        if (ImageLoadingHelper.isStoragePermissionGranted(ImageSelectActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Intent intent = new Intent();
            intent.setType("video/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.select_video)), SELECT_VIDEO_REQ_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Bundle resultBundle = data.getExtras();

            switch (requestCode) {
                case SELECT_PICTURE_REQ_CODE:
                    Uri selectedImageUri = data.getData();
                    onPictureSelect(selectedImageUri);
                    break;

                case SELECT_VIDEO_REQ_CODE:
                    Uri selectedVideoUri = data.getData();
                    onVideoSelect(selectedVideoUri);
                    break;

                case DRAW_ACT_REQ_CODE:
                    DrawnPointsCoordinates dpc = resultBundle.getParcelable(DrawActivity.ACT_RESULT_DATA);
                    FrameItem frameItem = gridViewAdapterData.get(selectedFrameIndex);
                    frameItem.setDrawnPointsCoordinates(dpc);
                    break;

                case FRAME_CAPTURE_REQ_CODE:
                    ArrayList<String> frameUriStrings = resultBundle.getStringArrayList(VideoFrameCaptureActivity.ACT_RESULT_DATA);
                    addFramesByUriStrings(frameUriStrings);
                    break;

                case LOAD_PROJECT_REQ_CODE:
                    String projectName = resultBundle.getString(ProjectListActivity.ACT_RESULT_DATA);
                    restoreProjectFromFile(projectName);
                    break;

            }
        }
    }

    private void addFramesByUriStrings(List<String> uriStrings) {
        for (String uriStr : uriStrings) {
            Uri uri = Uri.parse(uriStr);
            onPictureSelect(uri);
        }
    }

    private void onPictureSelect(Uri selectedImageUri) {
        try {
            Bitmap bitmap = ImageLoadingHelper.getThumbnailBitmapFromUri(this, selectedImageUri);
            if (bitmap != null) {
                FrameItem frameItem = new FrameItem(bitmap, selectedImageUri);
                gridViewAdapterData.add(frameItem);
                gridViewAdapter.notifyDataSetChanged();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.wtf("BITMAP", "IOEx");
        }
    }

    private void onVideoSelect(Uri selectedVideoUri) {
        Intent frameCaptureIntent = new Intent(context, VideoFrameCaptureActivity.class);
        frameCaptureIntent.putExtra(EXTRA_VIDEO_URI, selectedVideoUri.toString());
        startActivityForResult(frameCaptureIntent, FRAME_CAPTURE_REQ_CODE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(SAVED_GRID_DATA, gridViewAdapterData);
        outState.putInt(SAVED_FRAME_INDEX, selectedFrameIndex);
        outState.putString(SAVED_CURRENT_PROJ_NAME, currentProjectName);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        selectedFrameIndex = savedInstanceState.getInt(SAVED_FRAME_INDEX);
        currentProjectName = savedInstanceState.getString(SAVED_CURRENT_PROJ_NAME);
        ArrayList<FrameItem> frameItems = savedInstanceState.getParcelableArrayList(SAVED_GRID_DATA);
        if (frameItems != null) {
            gridViewAdapterData.addAll(frameItems);
            gridViewAdapter.notifyDataSetChanged();
        }
        super.onRestoreInstanceState(savedInstanceState);
    }
}
