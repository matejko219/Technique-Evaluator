/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Drawing;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.DrawingTool;
import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Exceptions.PointNotIncludedException;
import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Protractor;
import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Ruler;
import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Skeleton.Exceptions.IncompleteSkeletonException;
import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Skeleton.Joints.ArmJoint;
import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Skeleton.Joints.LegJoint;
import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Skeleton.Skeleton;
import com.github.matejko219.techniqueevaluator.Evaluation.EvaluationUtils;
import com.github.matejko219.techniqueevaluator.Helpers.DialogHelper;
import com.github.matejko219.techniqueevaluator.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mateusz Tumidajewicz (matejko219) on 2017-09-11.
 */
public class DrawingView extends View {

    public enum Tool {
        SKELETON,
        RULER,
        PROTRACTOR
    }

    public enum Mode {
        ADD,
        EDIT,
        DELETE
    }

    private Bitmap mBitmap;
    private Canvas mCanvas;
    private Paint mBitmapPaint;
    private Paint skeletonCirclePaint;
    private Paint protractorCirclePaint;
    private Paint rulerCirclePaint;
    private Paint centOfGravPaint;
    private Paint skeletonPaint;
    private Paint protractorPaint;
    private Paint rulerPaint;
    private Paint rectPaint;
    private Paint textPaint;
    private final int textSize = 100;
    private Paint valueRectPaint;
    private Paint valueTextPaint;
    Point currentPoint;
    Point pointToEdit;
    final int TOUCH_TOLERANCE = 8;
    final int MOVE_TOUCH_TOLERANCE = 4;
    Mode selectedMode = Mode.ADD;
    Tool selectedTool = Tool.SKELETON;

    Skeleton skeleton = new Skeleton();

    List<Protractor> protractors = new ArrayList<>();
    Protractor currentProtractor = new Protractor();

    List<Ruler> rulers = new ArrayList<>();
    Ruler currentRuler = new Ruler();

    private ScaleGestureDetector mScaleDetector;
    private float mScaleFactor = 1.f;
    private float mPivotX = 0.f;
    private float mPivotY = 0.f;
    private Drawable imageDrawable;

    private Context activityContext;
    private double rulerScale = 0.f;

    private float correctionRatio = 1f;

    public DrawingView(Context context) {
        super(context);
        init(context);
    }

    public DrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        initViewFields();
        mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
    }

    private void initViewFields() {
        mBitmapPaint = new Paint(Paint.DITHER_FLAG);

        skeletonCirclePaint = initCirclePaint(Color.CYAN);
        rulerCirclePaint = initCirclePaint(Color.MAGENTA);
        protractorCirclePaint = initCirclePaint(Color.YELLOW);
        centOfGravPaint = initCirclePaint(Color.RED);

        skeletonPaint = initLinePaint(Color.GREEN);
        protractorPaint = initLinePaint(Color.BLUE);
        rulerPaint = initLinePaint(Color.LTGRAY);

        rectPaint = initRectPaint(Color.WHITE, 128);
        textPaint = initTextPaint(Color.BLACK, textSize);

        valueRectPaint = initRectPaint(Color.BLACK, 50);
        valueTextPaint = initTextPaint(Color.WHITE, 70);
    }

    private Paint initCirclePaint(@ColorInt int color) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(color);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeJoin(Paint.Join.MITER);
        paint.setStrokeWidth(4);
        return paint;
    }

    private Paint initLinePaint(@ColorInt int color) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setColor(color);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeWidth(12);
        return paint;
    }

    private Paint initRectPaint(@ColorInt int color, int alpha) {
        Paint paint = new Paint();
        paint.setColor(color);
        paint.setStyle(Paint.Style.FILL);
        paint.setAlpha(alpha);
        return paint;
    }

    private Paint initTextPaint(@ColorInt int color, int size) {
        Paint paint = new Paint();
        paint.setColor(color);
        paint.setStyle(Paint.Style.FILL);
        paint.setTextSize(size);
        return paint;
    }

    public void setSelectedBitmap(Bitmap bitmap) {
        imageDrawable = new BitmapDrawable(getResources(), bitmap);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
        refreshDrawnedTools();
    }

    private void drawAllAddedTools() {
        drawInfo();
        drawAllCircles();
        drawSkeleton();
        drawProtractors();
        drawRulers();
        if (promptForScale()) drawRuler(currentRuler);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.save();
        canvas.scale(mScaleFactor, mScaleFactor, mPivotX, mPivotY);

        canvas.drawColor(Color.WHITE);

        if (imageDrawable != null) {
            setDrawableBounds(canvas);
            imageDrawable.draw(canvas);
        }

        canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);

        canvas.restore();
    }

    private void setDrawableBounds(Canvas canvas) {
        Bitmap bitmap = getBitmap();
        int bitmapWidth = bitmap.getWidth();
        int bitmapHeight = bitmap.getHeight();
        int screenOrientation = getResources().getConfiguration().orientation;

        if (bitmapWidth > bitmapHeight || (bitmapWidth == bitmapHeight && screenOrientation == Configuration.ORIENTATION_PORTRAIT)) {
            float heightRatio = ((float) bitmapHeight) / (float) (bitmapWidth);
            int height = (int) (canvas.getWidth() * heightRatio);

            float correctionRatio = 1f;
            if (height > canvas.getHeight()) {
                correctionRatio = (float)canvas.getHeight() / (float)height;
                height = canvas.getHeight();
            }

            int left = (int)((canvas.getWidth() - (canvas.getWidth() * correctionRatio)) / 2);
            int top = (canvas.getHeight() - height) / 2;
            int right = (int)(canvas.getWidth() * correctionRatio) + left;
            imageDrawable.setBounds(left, top, right, top + height);

        } else {
            float widthRatio = ((float) bitmapWidth) / (float) (bitmapHeight);
            int width = (int) (canvas.getHeight() * widthRatio);
            int left = (canvas.getWidth() - width) / 2;
            imageDrawable.setBounds(left, 0, left + width, canvas.getHeight());
        }
    }

    private Bitmap getBitmap() {
        if (imageDrawable != null) return ((BitmapDrawable) imageDrawable).getBitmap();
        else return null;
    }

    private void touchStart(float x, float y) {
        currentPoint = new Point(x, y);

        if (selectedMode != Mode.ADD) {
            try {
                pointToEdit = getPointToEdit();
                refreshDrawnedTools();

            } catch (PointNotIncludedException e) {
                pointToEdit = null;
                e.printStackTrace();
                Log.wtf("GetPointToEdit", e.getMessage());
            }
        }
    }

    private Point getPointToEdit() throws PointNotIncludedException {
        switch (selectedTool) {
            case SKELETON:
                return getDrawingToolPointToEdit(skeleton, null);
            case PROTRACTOR:
                return getDrawingToolPointToEdit(currentProtractor, protractors);
            case RULER:
                return getDrawingToolPointToEdit(currentRuler, rulers);
        }

        throw new PointNotIncludedException(selectedTool, currentPoint);
    }

    private Point getDrawingToolPointToEdit(DrawingTool currentTool, @Nullable List<? extends DrawingTool> tools) throws PointNotIncludedException {
        try {
            return currentTool.getPointToEdit(currentPoint);
        } catch (PointNotIncludedException e) {
            e.printStackTrace();
        }

        if (tools != null) {
            for (DrawingTool tool : tools) {
                try {
                    return tool.getPointToEdit(currentPoint);

                } catch (PointNotIncludedException e) {
                    e.printStackTrace();
                }
            }
        }

        throw new PointNotIncludedException(selectedTool, currentPoint);
    }

    private void touchMove(float x, float y) {
        float dx = Math.abs(x - currentPoint.x);
        float dy = Math.abs(y - currentPoint.y);

        if (dx >= MOVE_TOUCH_TOLERANCE || dy >= MOVE_TOUCH_TOLERANCE) {
            if (selectedMode == Mode.EDIT && pointToEdit != null) {

                pointToEdit.x = x;
                pointToEdit.y = y;

                if (selectedTool == Tool.SKELETON) skeleton.setNeckPoint();

                refreshDrawnedTools();

            } else if (!mScaleDetector.isInProgress()) {
                calculateDragPivot(x, y);
                refreshDrawnedTools();
            }
        }
    }

    private void refreshDrawnedTools() {
        if (mCanvas != null) mCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        drawAllAddedTools();
    }

    private void calculateDragPivot(float x, float y) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getDefaultDisplay().getRealMetrics(displayMetrics);

        float newPivotX = mPivotX - ((x - currentPoint.x) / 5f);
        if (newPivotX >= 0f && newPivotX <= displayMetrics.widthPixels) mPivotX = newPivotX;

        float newPivotY = mPivotY - ((y - currentPoint.y) / 5f);
        if (newPivotY >= 0f && newPivotY <= displayMetrics.heightPixels) mPivotY = newPivotY;
    }

    private void drawAllCircles() {
        drawAllSkeletonCircles();
        drawAllProtractorCircles();
        drawAllRulerCircles();
    }

    private void drawAllSkeletonCircles() {
        drawAllToolCircles(skeleton, null, skeletonCirclePaint);
    }

    private void drawAllProtractorCircles() {
        drawAllToolCircles(currentProtractor, protractors, protractorCirclePaint);
    }

    private void drawAllRulerCircles() {
        drawAllToolCircles(currentRuler, rulers, rulerCirclePaint);
    }

    private void drawAllToolCircles(DrawingTool currentTool, @Nullable List<? extends DrawingTool> tools, Paint paint) {
        for (Point point : currentTool.getAllEditablePoints()) {
            drawCircle(point, paint);
        }

        if (tools != null) {
            for (DrawingTool tool : tools) {
                for (Point point : tool.getAllEditablePoints()) {
                    drawCircle(point, paint);
                }
            }
        }
    }

    private void drawCircle(Point point, Paint paint) {
        int orientation = getResources().getConfiguration().orientation;
        int radius = orientation == Configuration.ORIENTATION_PORTRAIT ? EvaluationUtils.CIRCLE_RADIUS : EvaluationUtils.CIRCLE_RADIUS_SMALL;
        mCanvas.drawCircle(point.x, point.y, radius, paint);
    }

    private void touchUp(float x, float y) {
        switch (selectedMode) {
            case ADD:
                addPoint(x, y);
                break;
            case EDIT:
                pointToEdit = null;
                refreshDrawnedTools();
                break;
            case DELETE:
                if (isTouchToleranceValid(x, y) && pointToEdit != null) confirmDelete();
                break;
        }
    }

    private void addPoint(float x, float y) {
        switch (selectedTool) {
            case SKELETON:
                addSkeletonPoint(x, y);
                break;
            case PROTRACTOR:
                addProtractorPoint(x, y);
                break;
            case RULER:
                addRulerPoint(x, y);
        }

        refreshDrawnedTools();
    }

    private void addSkeletonPoint(float x, float y) {
        addToolPoint(skeleton, x, y);
    }

    private void addProtractorPoint(float x, float y) {
        if (addToolPoint(currentProtractor, x, y)) {
            if (currentProtractor.isComplete()) {
                protractors.add(currentProtractor);
                currentProtractor = new Protractor();
            }
        }
    }

    private void addRulerPoint(float x, float y) {
        if (addToolPoint(currentRuler, x, y)) {
            if (currentRuler.isComplete()) {
                if (promptForScale()) {
                    showScaleSetDialog();
                } else {
                    rulers.add(currentRuler);
                    currentRuler = new Ruler();
                }
            }
        }
    }

    private boolean addToolPoint(DrawingTool currentTool, float x, float y) {
        if (isTouchToleranceValid(x, y) && currentTool.canAddAnotherPoint()) {
            Log.d("Point", currentPoint.toString());

            try {
                currentTool.addPoint(currentPoint);
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("addToolPoint", e.getMessage());
            }

            return true;

        } else return false;
    }

    private void confirmDelete() {
        DialogHelper.showConfirm(activityContext, activityContext.getString(R.string.confirm_tool_delete), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deletePoint();
            }
        });
    }

    private void deletePoint() {
        switch (selectedTool) {
            case SKELETON:
                deleteSkeleton();
                break;
            case PROTRACTOR:
                deleteProtractor();
                break;
            case RULER:
                deleteRuler();
        }
    }

    private void deleteSkeleton() {
        if (skeleton.containsPoint(pointToEdit)) {
            skeleton = new Skeleton();
            refreshDrawnedTools();
        }
    }

    private void deleteProtractor() {
        if (currentProtractor.containsPoint(pointToEdit)) {
            currentProtractor = new Protractor();
            refreshDrawnedTools();

        } else deleteDrawingTool(protractors);
    }

    private void deleteRuler() {
        if (currentRuler.containsPoint(pointToEdit)) {
            currentRuler = new Ruler();
            refreshDrawnedTools();

        } else deleteDrawingTool(rulers);
    }

    private void deleteDrawingTool(List<? extends DrawingTool> tools) {
        for (DrawingTool tool : tools) {
            if (tool.containsPoint(pointToEdit)) {
                tools.remove(tool);
                refreshDrawnedTools();
                return;
            }
        }
    }

    private void drawProtractors() {
        for (Protractor protractor : protractors) {
            drawProtractor(protractor);
        }
    }

    private void drawProtractor(Protractor protractor) {
        if (protractor.isComplete()) {
            connectPoints(protractor.getFirst(), protractor.getFixed(), protractorPaint);
            connectPoints(protractor.getFixed(), protractor.getSecond(), protractorPaint);
            drawProtractorAngle(protractor);
        }
    }

    private void drawRulers() {
        for (Ruler ruler : rulers) {
            drawRuler(ruler);
        }
    }

    private void drawRuler(Ruler ruler) {
        if (ruler.isComplete()) {
            connectPoints(ruler.getFirst(), ruler.getSecond(), rulerPaint);
            drawRulerLength(ruler);
        }
    }

    private void drawRulerLength(Ruler ruler) {
        String rulerLengthTxt = String.format("%.3f [m]", ruler.getLength(rulerScale));
        float centerX = (ruler.getFirst().x + ruler.getSecond().x) / 2;
        float centerY = (ruler.getFirst().y + ruler.getSecond().y) / 2;
        drawToolValue(new Point(centerX, centerY), rulerLengthTxt);
    }

    private void drawProtractorAngle(Protractor protractor) {
        String protractorAngleTxt = String.format("%.2f°", protractor.getAngle());
        drawToolValue(protractor.getFixed(), protractorAngleTxt);
    }

    private void drawSkeletonAngle(Point centerPoint, float angle) {
        String skeletonAngleTxt = String.format("%.2f°", angle);
        drawToolValue(centerPoint, skeletonAngleTxt);
    }

    private void drawToolValue(Point centerPoint, String value) {
        int left = (int)(centerPoint.x - (valueTextPaint.measureText(value) + 20) / 2);
        int top = (int)(centerPoint.y - 35);
        int right = (int)(centerPoint.x + (valueTextPaint.measureText(value) + 20) / 2);
        int bottom = (int)(centerPoint.y + 35);

        mCanvas.drawRoundRect(left, top, right, bottom, 10, 10, valueRectPaint);
        mCanvas.drawText(value, left + 10, bottom - 12, valueTextPaint);
    }

    private void drawInfo() {
        String infoTxt = getInfoText();
        if (infoTxt != null && mCanvas != null) {
            float x = scaleAndTranslateX(0f);
            float y = scaleAndTranslateY(0f);

            float rectRight = x + textPaint.measureText(infoTxt) + 20;
            float rectBottom = y + (100 / mScaleFactor);
            float borderRadius = 10 / mScaleFactor;
            mCanvas.drawRoundRect(x, y, rectRight, rectBottom, borderRadius, borderRadius, rectPaint);

            float textX = x + 10;
            float textY = y + (85  / mScaleFactor);
            mCanvas.drawText(infoTxt, textX, textY, textPaint);
        }
    }

    private String getInfoText() {
        if (selectedTool == Tool.SKELETON) return getSkeletonInfoText();
        else return null;
    }

    private String getSkeletonInfoText() {
        if (selectedMode == Mode.ADD) {
            return skeleton.isComplete() ? null : EvaluationUtils.skeletonPointsOrder.get(skeleton.getNumberOfPoints());

        } else if (selectedMode == Mode.EDIT && pointToEdit != null) {
            int i = 0;
            for (Point point : skeleton.getAllEditablePoints()) {
                if (point == pointToEdit) {
                    return EvaluationUtils.skeletonPointsOrder.get(i);
                }
                i++;
            }
        }

        return null;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mScaleDetector.onTouchEvent(event);

        float x = scaleAndTranslateX(event.getX());

        float y = scaleAndTranslateY(event.getY());

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touchStart(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                touchMove(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                touchUp(x, y);
                invalidate();
                break;
        }
        return true;
    }

    private float scaleAndTranslateX(float x) {
        x -= mPivotX;
        x /= mScaleFactor;
        x += mCanvas.getClipBounds().left;
        x += mPivotX;
        return x;
    }

    private float scaleAndTranslateY(float y) {
        y -= mPivotY;
        y /= mScaleFactor;
        y += mCanvas.getClipBounds().top;
        y += mPivotY;
        return y;
    }

    private void drawSkeleton() {
        if (skeleton.isComplete()) {
            drawArm(skeleton.getLeftArmJoint());
            drawArm(skeleton.getRightArmJoint());
            drawLeg(skeleton.getLeftLegJoint());
            drawLeg(skeleton.getRightLegJoint());
            drawHead(skeleton);
            connectJoints(skeleton);
            try {
                drawCenterOfGravity(skeleton.getCenterOfGravityPoint());
            } catch (IncompleteSkeletonException e) {
                e.printStackTrace();
                Log.wtf("DrawCenterOfGrav", e.getMessage());
            }
        }
    }

    private void drawArm(ArmJoint armJoint) {
        Point shoulder = armJoint.getShoulder();
        Point elbow = armJoint.getElbow();
        Point wrist = armJoint.getWrist();

        connectPoints(shoulder, elbow);
        connectPoints(elbow, wrist);

        drawSkeletonAngle(elbow, armJoint.getInnerElbowAngle());
    }

    private void drawLeg(LegJoint legJoint) {
        Point hip = legJoint.getHip();
        Point knee = legJoint.getKnee();
        Point ankle = legJoint.getAnkle();
        Point foot = legJoint.getFoot();

        connectPoints(hip, knee);
        connectPoints(knee, ankle);
        connectPoints(ankle, foot);

        drawSkeletonAngle(knee, legJoint.getInnerKneeAngle());
    }

    private void drawHead(Skeleton skeleton) {
        Point head = skeleton.getHeadJoint().getHead();
        Point neck = skeleton.getHeadJoint().getNeck();

        connectPoints(head, neck);
    }

    private void connectPoints(Point a, Point b) {
        mCanvas.drawLine(a.x, a.y, b.x, b.y, skeletonPaint);
    }

    private void connectPoints(Point a, Point b, Paint paint) {
        mCanvas.drawLine(a.x, a.y, b.x, b.y, paint);
    }

    private void connectJoints(Skeleton skeleton) {
        Point leftShoulder = skeleton.getLeftArmJoint().getShoulder();
        Point rightShoulder = skeleton.getRightArmJoint().getShoulder();
        Point leftHip = skeleton.getLeftLegJoint().getHip();
        Point rightHip = skeleton.getRightLegJoint().getHip();

        connectPoints(leftShoulder, rightShoulder);
        connectPoints(leftHip, rightHip);
        connectPoints(leftShoulder, rightHip);
        connectPoints(rightShoulder, leftHip);
    }

    private void drawCenterOfGravity(Point centerOfGravity) {
        if (centerOfGravity != null)
            drawCircle(centerOfGravity, centOfGravPaint);
    }

    public void resetView() {
        skeleton = new Skeleton();
        currentProtractor = new Protractor();
        protractors = new ArrayList<>();
        currentRuler = new Ruler();
        rulers = new ArrayList<>();
        rulerScale = 0.f;
        showBeforeScaleSetInfoIfNeeded();
        refreshDrawnedTools();
    }

    private boolean isTouchToleranceValid(float x, float y) {
        if (currentPoint != null) {

            float dx = Math.abs(x - currentPoint.x);
            float dy = Math.abs(y - currentPoint.y);

            return dx <= TOUCH_TOLERANCE && dy <= TOUCH_TOLERANCE;

        } else return false;
    }

    public void setMode(Mode mode) {
        this.selectedMode = mode;
        showBeforeScaleSetInfoIfNeeded();
        refreshDrawnedTools();
    }

    public void setTool(Tool tool) {
        selectedTool = tool;
        showBeforeScaleSetInfoIfNeeded();
        refreshDrawnedTools();
    }

    private void showBeforeScaleSetInfoIfNeeded() {
        if (promptForScale()) DialogHelper.showBeforeScaleSetInfo(activityContext);
    }

    private boolean promptForScale() {
        return selectedTool == Tool.RULER && selectedMode == Mode.ADD && rulerScale == 0.f;
    }

    private void showScaleSetDialog() {
        final EditText scaleInput = new EditText(activityContext);
        scaleInput.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

        AlertDialog.Builder builder = new AlertDialog.Builder(activityContext);
        builder
                .setTitle(R.string.length_prompt)
                .setView(scaleInput)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            String scaleInputValue = scaleInput.getText().toString();
                            float parsedScale = Float.parseFloat(scaleInputValue);
                            if (parsedScale <= 0.f) throw new IllegalArgumentException();
                            double rulerDistance = EvaluationUtils.distanceBetween2Points(currentRuler.getFirst(), currentRuler.getSecond());
                            rulerScale = parsedScale / rulerDistance;

                        } catch (Exception e) {
                            e.printStackTrace();
                            DialogHelper.showError(activityContext, activityContext.getString(R.string.scale_value_error));
                        }

                        clearScaleRuler();
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        clearScaleRuler();
                    }
                })
                .show();
    }

    private void clearScaleRuler() {
        currentRuler = new Ruler();
        refreshDrawnedTools();
    }

    public void setActivityContext(Context context) {
        activityContext = context;
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        DrawingViewState drawingViewState = new DrawingViewState(superState);
        drawingViewState.skeleton = skeleton;
        drawingViewState.selectedBitmap = imageDrawable;
        drawingViewState.selectedTool = selectedTool;
        drawingViewState.currentProtractor = currentProtractor;
        drawingViewState.protractors = protractors;
        drawingViewState.currentRuler = currentRuler;
        drawingViewState.rulers = rulers;
        drawingViewState.rulerScale = rulerScale;
        drawingViewState.correctionRatio = correctionRatio;
        return drawingViewState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (!(state instanceof DrawingViewState)) {
            super.onRestoreInstanceState(state);
            return;
        }

        DrawingViewState drawingViewState = (DrawingViewState) state;
        super.onRestoreInstanceState(drawingViewState.getSuperState());

        skeleton = drawingViewState.skeleton;
        imageDrawable = drawingViewState.selectedBitmap;
        selectedTool = drawingViewState.selectedTool;
        currentProtractor = drawingViewState.currentProtractor;
        protractors = drawingViewState.protractors;
        currentRuler = drawingViewState.currentRuler;
        rulers = drawingViewState.rulers;
        rulerScale = drawingViewState.rulerScale;
        correctionRatio = drawingViewState.correctionRatio;

        scalePointsToViewport();
    }

    private void scalePointsToViewport() {
        Bitmap bitmap = getBitmap();

        if (bitmap != null) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getDefaultDisplay().getRealMetrics(displayMetrics);

            float ratio;
            if (bitmap.getWidth() > bitmap.getHeight()) {
                ratio = (float) displayMetrics.widthPixels / (float) displayMetrics.heightPixels;
                float heightRatio = ((float) bitmap.getHeight()) / (float) (bitmap.getWidth());
                int height = (int) (displayMetrics.widthPixels * heightRatio);

                if (height > displayMetrics.heightPixels) {
                    this.correctionRatio = (float)displayMetrics.heightPixels / (float)height;
                    ratio *= this.correctionRatio;

                } else if (this.correctionRatio != 1f) {
                    ratio /= this.correctionRatio;
                }
            } else {
                ratio = (float) displayMetrics.heightPixels / (float) displayMetrics.widthPixels;
            }

            rulerScale /= ratio;
            scaleSkeletonPoints(displayMetrics, ratio);
            scaleProtractorPoints(displayMetrics, ratio);
            scaleRulerPoints(displayMetrics, ratio);
        }
    }

    private void scaleSkeletonPoints(DisplayMetrics displayMetrics, float ratio) {
        scaleToolPoints(skeleton, null, displayMetrics, ratio);
        skeleton.setNeckPoint();
    }

    private void scaleProtractorPoints(DisplayMetrics displayMetrics, float ratio) {
        scaleToolPoints(currentProtractor, protractors, displayMetrics, ratio);
    }

    private void scaleRulerPoints(DisplayMetrics displayMetrics, float ratio) {
        scaleToolPoints(currentRuler, rulers, displayMetrics, ratio);
    }

    private void scaleToolPoints(DrawingTool currentTool, @Nullable List<? extends DrawingTool> tools, DisplayMetrics displayMetrics, float ratio) {
        for (Point point : currentTool.getAllEditablePoints()) {
            Point scaledPoint = scalePoint(point, displayMetrics, ratio);
            point.x = scaledPoint.x;
            point.y = scaledPoint.y;
        }

        if (tools != null) {
            for (DrawingTool tool : tools) {
                for (Point point : tool.getAllEditablePoints()) {
                    Point scaledPoint = scalePoint(point, displayMetrics, ratio);
                    point.x = scaledPoint.x;
                    point.y = scaledPoint.y;
                }
            }
        }
    }

    private Point scalePoint(Point point, DisplayMetrics displayMetrics, float ratio) {
        Matrix matrix = new Matrix();
        matrix.postScale(1, -1, displayMetrics.widthPixels / 2f, displayMetrics.heightPixels / 2f);
        matrix.postScale(ratio, ratio, displayMetrics.widthPixels / 2f, displayMetrics.heightPixels / 2f);
        matrix.postRotate(90, displayMetrics.widthPixels / 2f, displayMetrics.heightPixels / 2f);
        float[] pts = new float[]{point.y, point.x};
        matrix.mapPoints(pts);
        return new Point(pts[0], pts[1]);
    }

    private Display getDefaultDisplay() {
        WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        return windowManager.getDefaultDisplay();
    }

    public DrawnPointsCoordinates getDrawnPointsCoordinates() {
        int orientation = getResources().getConfiguration().orientation;
        return new DrawnPointsCoordinates(skeleton, currentProtractor, protractors, currentRuler, rulers, orientation, rulerScale, correctionRatio);
    }

    public void setDrawnPointsCoordinates(DrawnPointsCoordinates dpc) {
        skeleton = dpc.getSkeleton();
        currentProtractor = dpc.getCurrentProtractor();
        protractors = dpc.getProtractors();
        currentRuler = dpc.getCurrentRuler();
        rulers = dpc.getRulers();
        rulerScale = dpc.getRulerScale();
        correctionRatio = dpc.getCorrectionRatio();

        int dpcOrientation = dpc.getOrientation();
        int screenOrientation = getResources().getConfiguration().orientation;

        if (dpcOrientation != 0 && dpcOrientation != screenOrientation) {
            scalePointsToViewport();
        }
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            mScaleFactor *= detector.getScaleFactor();

            mScaleFactor = Math.max(1.f, Math.min(mScaleFactor, 5.0f));
            if (mScaleFactor == 1.f) {
                mPivotX = 0.f;
                mPivotY = 0.f;
                textPaint.setTextSize(textSize);
            } else {
                mPivotX = detector.getFocusX();
                mPivotY = detector.getFocusY();
                textPaint.setTextSize(textSize / mScaleFactor);
            }

            return true;
        }
    }
}
