/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Helpers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Protractor;
import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Ruler;
import com.github.matejko219.techniqueevaluator.Drawing.DrawnPointsCoordinates;
import com.github.matejko219.techniqueevaluator.R;

/**
 * Created by Mateusz Tumidajewicz (matejko219) on 2018-03-05.
 */
public class DialogHelper {

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static void showConfirm(Context context, String msg, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder
                .setMessage(msg)
                .setPositiveButton(context.getString(R.string.yes), onClickListener)
                .setNegativeButton(context.getString(R.string.no), null)
                .show();
    }

    public static void showError(Context context, String msg) {
        showInfo(context, context.getString(R.string.error), msg);
    }

    public static void showBeforeScaleSetInfo(Context context) {
        showInfo(context, null, String.format("%s %s", context.getString(R.string.before_scale_1),
                context.getString(R.string.before_scale_2)));
    }

    public static void showResults(Context context, DrawnPointsCoordinates dpc) {
        showInfo(context, context.getString(R.string.results), getResultMsg(context, dpc));
    }

    private static void showInfo(Context context, @Nullable String title, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton("OK", null)
                .show();
    }

    private static String getResultMsg(Context context, DrawnPointsCoordinates dpc) {
        StringBuilder resultStringBuilder = new StringBuilder();

        if (dpc.getSkeleton().isComplete()) {
            resultStringBuilder.append(String.format(context.getString(R.string.skeleton) + ":\n" +
                            context.getString(R.string.left_elbow) + ": %.2f\u00b0\n" +
                            context.getString(R.string.right_elbow) + ": %.2f\u00b0\n" +
                            context.getString(R.string.left_knee) + ": %.2f\u00b0\n" +
                            context.getString(R.string.right_knee) + ": %.2f\u00b0\n\n",
                    dpc.getSkeleton().getLeftArmJoint().getInnerElbowAngle(),
                    dpc.getSkeleton().getRightArmJoint().getInnerElbowAngle(),
                    dpc.getSkeleton().getLeftLegJoint().getInnerKneeAngle(),
                    dpc.getSkeleton().getRightLegJoint().getInnerKneeAngle()));
        }

        int protractorIdx = 1;
        for (Protractor protractor : dpc.getProtractors()) {
            if (protractor.isComplete()) {
                resultStringBuilder.append(String.format(context.getString(R.string.protractor) + " %d: %.2f\u00b0\n",
                        protractorIdx,
                        protractor.getAngle()));
                protractorIdx++;
            }
        }

        if (protractorIdx > 1) resultStringBuilder.append("\n");

        int rulerIdx = 1;
        for (Ruler ruler : dpc.getRulers()) {
            if (ruler.isComplete()) {
                resultStringBuilder.append(String.format(context.getString(R.string.ruler) + " %d: %.3f [m]\n",
                        rulerIdx,
                        ruler.getLength(dpc.getRulerScale())));
                rulerIdx++;
            }
        }

        return resultStringBuilder.toString();
    }
}
