/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.github.matejko219.techniqueevaluator.Adapters.ListViewAdapter;
import com.github.matejko219.techniqueevaluator.Adapters.ProjectFile;
import com.github.matejko219.techniqueevaluator.Helpers.DialogHelper;
import com.github.matejko219.techniqueevaluator.Helpers.FileHelper;
import com.github.matejko219.techniqueevaluator.Helpers.SaveRestoreHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class ProjectListActivity extends AppCompatActivity {

    private ArrayList<ProjectFile> projectFileList;
    private ListViewAdapter listViewAdapter;
    public static final String ACT_RESULT_DATA = "PROJECT_RESULT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_list);

        ListView listView = (ListView) findViewById(R.id.project_files_list);
        projectFileList = new ArrayList<>(getProjectFiles());
        listViewAdapter = new ListViewAdapter(ProjectListActivity.this, R.layout.list_item_layout, projectFileList);
        listView.setAdapter(listViewAdapter);
        registerForContextMenu(listView);
        TextView emptyInfo = (TextView)findViewById(R.id.empty_list_info);
        listView.setEmptyView(emptyInfo);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                loadProject(position);
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.list_item_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()) {
            case R.id.action_list_item_load:
                loadProject(info.position);
                return true;
            case R.id.action_list_item_delete:
                deleteProject(info.position);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void loadProject(int position) {
        Bundle resultBundle = new Bundle();
        resultBundle.putString(ACT_RESULT_DATA, projectFileList.get(position).getName());
        Intent resultIntent = new Intent();
        resultIntent.putExtras(resultBundle);
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    private void deleteProject(int position) {
        final String projectName = projectFileList.get(position).getName();

        DialogHelper.showConfirm(ProjectListActivity.this, String.format(getString(R.string.project_delete_confirm), projectName), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (SaveRestoreHelper.deleteProjectDirectory(getContentResolver(), projectName))
                    DialogHelper.showToast(ProjectListActivity.this, String.format(getString(R.string.project_deleted), projectName));
                else DialogHelper.showToast(ProjectListActivity.this, String.format(getString(R.string.project_delete_error), projectName));

                refreshProjectList();
            }
        });
    }

    private void refreshProjectList() {
        projectFileList.clear();
        projectFileList.addAll(getProjectFiles());
        listViewAdapter.notifyDataSetChanged();
    }

    private List<ProjectFile> getProjectFiles() {
        List<ProjectFile> projectFiles = new ArrayList<>();
        File savedProjectsDirectory = FileHelper.getAppDirectory(FileHelper.AppDirectory.SAVED_PROJECTS);

        File[] files = savedProjectsDirectory.listFiles();
        Arrays.sort(files, new Comparator<File>() {
            @Override
            public int compare(File lhs, File rhs) {
                return Long.compare(rhs.lastModified(), lhs.lastModified());
            }
        });

        for (File file : files) {
            if (file.isDirectory()) {
                for (File child : file.listFiles()) {
                    if (child.isFile() && child.getName().equals(SaveRestoreHelper.projectFileNamePattern(file.getName()))) {
                        ProjectFile projectFile = new ProjectFile(file.getName(), child.lastModified());
                        projectFiles.add(projectFile);
                        break;
                    }
                }
            }
        }

        return projectFiles;
    }

}
