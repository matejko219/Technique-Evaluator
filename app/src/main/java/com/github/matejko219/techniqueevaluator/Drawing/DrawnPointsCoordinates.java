/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Drawing;

import android.os.Parcel;
import android.os.Parcelable;

import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Protractor;
import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Ruler;
import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Skeleton.Skeleton;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DELL on 2018-02-10.
 */
public class DrawnPointsCoordinates implements Parcelable, Serializable {

    private Skeleton skeleton;
    private Protractor currentProtractor;
    private List<Protractor> protractors;
    private Ruler currentRuler;
    private List<Ruler> rulers;
    private int orientation;
    private double rulerScale;
    private float correctionRatio;

    public DrawnPointsCoordinates() {
        this.skeleton = new Skeleton();
        this.currentProtractor = new Protractor();
        this.protractors = new ArrayList<>();
        this.currentRuler = new Ruler();
        this.rulers = new ArrayList<>();
    }

    public DrawnPointsCoordinates(Skeleton skeleton,
                                  Protractor currentProtractor,
                                  List<Protractor> protractors,
                                  Ruler currentRuler,
                                  List<Ruler> rulers,
                                  int orientation,
                                  double rulerScale,
                                  float correctionRatio) {
        this.skeleton = skeleton;
        this.currentProtractor = currentProtractor;
        this.protractors = protractors;
        this.currentRuler = currentRuler;
        this.rulers = rulers;
        this.orientation = orientation;
        this.rulerScale = rulerScale;
        this.correctionRatio = correctionRatio;
    }

    protected DrawnPointsCoordinates(Parcel in) {
        skeleton = in.readParcelable(Skeleton.class.getClassLoader());
        currentProtractor = in.readParcelable(Protractor.class.getClassLoader());
        protractors = new ArrayList<>();
        in.readList(protractors, Protractor.class.getClassLoader());
        currentRuler = in.readParcelable(Ruler.class.getClassLoader());
        rulers = new ArrayList<>();
        in.readList(rulers, Ruler.class.getClassLoader());
        orientation = in.readInt();
        rulerScale = in.readDouble();
        correctionRatio = in.readFloat();
    }

    public Skeleton getSkeleton() {
        return skeleton;
    }

    public void setSkeleton(Skeleton skeleton) {
        this.skeleton = skeleton;
    }

    public int getOrientation() {
        return orientation;
    }

    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }

    public Protractor getCurrentProtractor() {
        return currentProtractor;
    }

    public void setCurrentProtractor(Protractor currentProtractor) {
        this.currentProtractor = currentProtractor;
    }

    public List<Protractor> getProtractors() {
        return protractors;
    }

    public void setProtractors(List<Protractor> protractors) {
        this.protractors = protractors;
    }

    public Ruler getCurrentRuler() {
        return currentRuler;
    }

    public void setCurrentRuler(Ruler currentRuler) {
        this.currentRuler = currentRuler;
    }

    public List<Ruler> getRulers() {
        return rulers;
    }

    public void setRulers(List<Ruler> rulers) {
        this.rulers = rulers;
    }

    public double getRulerScale() {
        return rulerScale;
    }

    public void setRulerScale(double rulerScale) {
        this.rulerScale = rulerScale;
    }

    public float getCorrectionRatio() {
        return correctionRatio;
    }

    public void setCorrectionRatio(float correctionRatio) {
        this.correctionRatio = correctionRatio;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(skeleton, flags);
        dest.writeParcelable(currentProtractor, flags);
        dest.writeList(protractors);
        dest.writeParcelable(currentRuler, flags);
        dest.writeList(rulers);
        dest.writeInt(orientation);
        dest.writeDouble(rulerScale);
        dest.writeFloat(correctionRatio);
    }

    public static final Creator<DrawnPointsCoordinates> CREATOR = new Creator<DrawnPointsCoordinates>() {
        @Override
        public DrawnPointsCoordinates createFromParcel(Parcel in) {
            return new DrawnPointsCoordinates(in);
        }

        @Override
        public DrawnPointsCoordinates[] newArray(int size) {
            return new DrawnPointsCoordinates[size];
        }
    };
}
