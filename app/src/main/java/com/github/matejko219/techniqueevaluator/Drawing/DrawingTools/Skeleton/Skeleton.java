/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Skeleton;

import android.os.Parcel;
import android.os.Parcelable;

import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.DrawingTool;
import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Exceptions.TooManyPointsException;
import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Skeleton.Exceptions.IncompleteSkeletonException;
import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Skeleton.Joints.ArmJoint;
import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Skeleton.Joints.HeadJoint;
import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Skeleton.Joints.LegJoint;
import com.github.matejko219.techniqueevaluator.Drawing.DrawingView;
import com.github.matejko219.techniqueevaluator.Drawing.Point;
import com.github.matejko219.techniqueevaluator.Evaluation.EvaluationUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DELL on 2017-09-22.
 */
public class Skeleton extends DrawingTool implements Parcelable, Serializable {
    private HeadJoint headJoint;
    private ArmJoint leftArmJoint;
    private ArmJoint rightArmJoint;
    private LegJoint leftLegJoint;
    private LegJoint rightLegJoint;
    private boolean isLeftShoulderSet;
    private boolean isRightShoulderSet;

    public Skeleton() {
        super(EvaluationUtils.getMaxSkeletonNumberOfPoints(), DrawingView.Tool.SKELETON);
        headJoint = new HeadJoint();
        leftArmJoint = new ArmJoint();
        rightArmJoint = new ArmJoint();
        leftLegJoint = new LegJoint();
        rightLegJoint = new LegJoint();
    }

    protected Skeleton(Parcel in) {
        super(in, EvaluationUtils.getMaxSkeletonNumberOfPoints(), DrawingView.Tool.SKELETON);
        isLeftShoulderSet = in.readByte() != 0;
        isRightShoulderSet = in.readByte() != 0;
        headJoint = in.readParcelable(HeadJoint.class.getClassLoader());
        leftArmJoint = in.readParcelable(ArmJoint.class.getClassLoader());
        rightArmJoint = in.readParcelable(ArmJoint.class.getClassLoader());
        leftLegJoint = in.readParcelable(LegJoint.class.getClassLoader());
        rightLegJoint = in.readParcelable(LegJoint.class.getClassLoader());
    }

    @Override
    public void addPoint(Point point) throws TooManyPointsException {
        if (!canAddAnotherPoint()) {
            throw new TooManyPointsException(DrawingView.Tool.SKELETON, super.maxNumberOfPoints, numberOfPoints);
        }
        String partType = EvaluationUtils.skeletonPointsOrder.get(numberOfPoints);
        addPointByType(point, partType);
        numberOfPoints++;
        setNeckPoint();
    }

    private void addPointByType(Point point, String type) {
        switch (type) {
            case EvaluationUtils.HEAD:
                headJoint.setHead(point);
                break;
            case EvaluationUtils.LEFT_SHOULDER:
                leftArmJoint.setShoulder(point);
                isLeftShoulderSet = true;
                break;
            case EvaluationUtils.LEFT_ELBOW:
                leftArmJoint.setElbow(point);
                break;
            case EvaluationUtils.LEFT_WRIST:
                leftArmJoint.setWrist(point);
                break;
            case EvaluationUtils.RIGHT_SHOULDER:
                rightArmJoint.setShoulder(point);
                isRightShoulderSet = true;
                break;
            case EvaluationUtils.RIGHT_ELBOW:
                rightArmJoint.setElbow(point);
                break;
            case EvaluationUtils.RIGHT_WRIST:
                rightArmJoint.setWrist(point);
                break;
            case EvaluationUtils.LEFT_HIP:
                leftLegJoint.setHip(point);
                break;
            case EvaluationUtils.LEFT_KNEE:
                leftLegJoint.setKnee(point);
                break;
            case EvaluationUtils.LEFT_ANKLE:
                leftLegJoint.setAnkle(point);
                break;
            case EvaluationUtils.LEFT_FOOT:
                leftLegJoint.setFoot(point);
                break;
            case EvaluationUtils.RIGHT_HIP:
                rightLegJoint.setHip(point);
                break;
            case EvaluationUtils.RIGHT_KNEE:
                rightLegJoint.setKnee(point);
                break;
            case EvaluationUtils.RIGHT_ANKLE:
                rightLegJoint.setAnkle(point);
                break;
            case EvaluationUtils.RIGHT_FOOT:
                rightLegJoint.setFoot(point);
                break;
        }
    }

    public HeadJoint getHeadJoint() {
        return headJoint;
    }

    public ArmJoint getLeftArmJoint() {
        return leftArmJoint;
    }

    public ArmJoint getRightArmJoint() {
        return rightArmJoint;
    }

    public LegJoint getLeftLegJoint() {
        return leftLegJoint;
    }

    public LegJoint getRightLegJoint() {
        return rightLegJoint;
    }

    private List<Point> getAllPoints() {
        List<Point> allPoints = new ArrayList<>();

        allPoints.addAll(headJoint.getPointList());
        allPoints.addAll(leftArmJoint.getPointList());
        allPoints.addAll(rightArmJoint.getPointList());
        allPoints.addAll(leftLegJoint.getPointList());
        allPoints.addAll(rightLegJoint.getPointList());

        return allPoints;
    }

    @Override
    public List<Point> getAllEditablePoints() {
        List<Point> allEditablePoints = getAllPoints();

        // usunięcie "szyi" ponieważ jest to punkt automatycznie wyliczany
        // a nie podawany przez użytkownika
        if (headJoint.getNeck() != null) {
            allEditablePoints.remove(headJoint.getNeck());
        }

        return allEditablePoints;
    }

    public void setNeckPoint() {
        if (isLeftShoulderSet && isRightShoulderSet) {
            headJoint.setNeckFromShoulders(leftArmJoint.getShoulder(), rightArmJoint.getShoulder());
        }
    }

    public Point getCenterOfGravityPoint() throws IncompleteSkeletonException {
        Point result = new Point(0.0f, 0.0f);
        List<Point> allPoints = getAllEditablePoints();
        int size = allPoints.size();

        if (size == super.maxNumberOfPoints) {
            for (Point point : allPoints) {
                result.x += point.x;
                result.y += point.y;
            }

            result.x /= allPoints.size();
            result.y /= allPoints.size();

            return result;

        } else throw new IncompleteSkeletonException(size);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeByte((byte) (isLeftShoulderSet ? 1 : 0));
        dest.writeByte((byte) (isRightShoulderSet ? 1 : 0));
        dest.writeParcelable(headJoint, flags);
        dest.writeParcelable(leftArmJoint, flags);
        dest.writeParcelable(rightArmJoint, flags);
        dest.writeParcelable(leftLegJoint, flags);
        dest.writeParcelable(rightLegJoint, flags);
    }

    public static final Creator<Skeleton> CREATOR = new Creator<Skeleton>() {
        @Override
        public Skeleton createFromParcel(Parcel in) {
            return new Skeleton(in);
        }

        @Override
        public Skeleton[] newArray(int size) {
            return new Skeleton[size];
        }
    };
}
