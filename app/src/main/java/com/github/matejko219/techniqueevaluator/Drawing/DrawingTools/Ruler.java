/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Drawing.DrawingTools;

import android.os.Parcel;
import android.os.Parcelable;

import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Exceptions.TooManyPointsException;
import com.github.matejko219.techniqueevaluator.Drawing.DrawingView;
import com.github.matejko219.techniqueevaluator.Drawing.Point;
import com.github.matejko219.techniqueevaluator.Evaluation.EvaluationUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DELL on 2018-03-03.
 */
public class Ruler extends DrawingTool implements Parcelable, Serializable {

    Point first;
    Point second;

    public Ruler() {
        super(2, DrawingView.Tool.RULER);
    }

    protected Ruler(Parcel in) {
        super(in, 2, DrawingView.Tool.RULER);
        first = in.readParcelable(Point.class.getClassLoader());
        second = in.readParcelable(Point.class.getClassLoader());
    }

    public Point getFirst() {
        return first;
    }

    public Point getSecond() {
        return second;
    }

    @Override
    public List<Point> getAllEditablePoints() {
        List<Point> allEditablePoints = new ArrayList<>();

        if (first != null) allEditablePoints.add(first);
        if (second != null) allEditablePoints.add(second);

        return allEditablePoints;
    }

    @Override
    public void addPoint(Point point) throws TooManyPointsException {
        if (!canAddAnotherPoint()) {
            throw new TooManyPointsException(super.tool, super.maxNumberOfPoints, super.numberOfPoints);
        }

        switch (numberOfPoints) {
            case 0:
                first = point;
                break;
            case 1:
                second = point;
                break;
            default:
                throw new TooManyPointsException(super.tool, maxNumberOfPoints, numberOfPoints);
        }

        numberOfPoints++;
    }

    public double getLength(double rulerScale) {
        double distance = EvaluationUtils.distanceBetween2Points(first, second);
        return distance * rulerScale;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(first, flags);
        dest.writeParcelable(second, flags);
    }

    public static final Creator<Ruler> CREATOR = new Creator<Ruler>() {
        @Override
        public Ruler createFromParcel(Parcel in) {
            return new Ruler(in);
        }

        @Override
        public Ruler[] newArray(int size) {
            return new Ruler[size];
        }
    };
}
