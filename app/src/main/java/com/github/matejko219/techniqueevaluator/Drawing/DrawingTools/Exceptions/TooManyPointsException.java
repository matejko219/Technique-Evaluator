/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Exceptions;

import com.github.matejko219.techniqueevaluator.Drawing.DrawingView;

/**
 * Created by DELL on 2017-09-22.
 */
public class TooManyPointsException extends Exception {

    public TooManyPointsException(DrawingView.Tool tool, int maxNumberOfPoints, int numberOfPoints) {
        super(String.format("Tried to insert %d point but %s can contain only %d points", numberOfPoints + 1, tool.name(), maxNumberOfPoints));
    }

}
