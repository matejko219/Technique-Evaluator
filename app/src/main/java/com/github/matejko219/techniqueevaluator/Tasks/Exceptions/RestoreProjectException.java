/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Tasks.Exceptions;

import android.app.Activity;

import com.github.matejko219.techniqueevaluator.R;

/**
 * Created by Mateusz Tumidajewicz (matejko219) on 2018-03-29.
 */
public class RestoreProjectException extends Exception {

    public RestoreProjectException(Activity activity, String fileName, Throwable throwable) {
        super(String.format(activity.getString(R.string.project_restore_error), fileName), throwable);
    }
}
