/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Tasks;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;

import com.github.matejko219.techniqueevaluator.Drawing.FrameItem;
import com.github.matejko219.techniqueevaluator.Helpers.FileHelper;
import com.github.matejko219.techniqueevaluator.Helpers.ImageLoadingHelper;
import com.github.matejko219.techniqueevaluator.Helpers.SaveRestoreHelper;
import com.github.matejko219.techniqueevaluator.Helpers.ScanFileCallback;
import com.github.matejko219.techniqueevaluator.Tasks.Exceptions.RestoreProjectException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Mateusz Tumidajewicz (matejko219) on 2018-03-29.
 */
public class RestoreProjectAsyncTask extends AsyncTask<String, Integer, Void> {

    private Activity activity;
    private RestoreProjectAsyncCallbackHandler callbackHandler;

    public RestoreProjectAsyncTask(Activity activity, RestoreProjectAsyncCallbackHandler callbackHandler) {
        this.activity = activity;
        this.callbackHandler = callbackHandler;
    }

    @Override
    protected Void doInBackground(String... params) {
        final String projectName = params[0];
        File fileToRead = SaveRestoreHelper.getProjectSaveFile(projectName);
        List<FrameItem> frameItemsFromFile = null;

        FileInputStream fis = null;
        ObjectInputStream ois = null;

        try {
            fis = new FileInputStream(fileToRead);
            ois = new ObjectInputStream(fis);
            frameItemsFromFile = (List<FrameItem>) ois.readObject();

        } catch (Exception e) {
            e.printStackTrace();
            callbackHandler.onException(new RestoreProjectException(activity, SaveRestoreHelper.projectFileNamePattern(projectName), e));
            return null;

        } finally {
            try {
                if (ois != null) ois.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                if (fis != null) fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        final ArrayList<FrameItem> frameItems = new ArrayList<>(frameItemsFromFile);
        final AtomicInteger executeCounter = new AtomicInteger();

        int i = 0;
        for (final FrameItem frameItem : frameItems) {
            File bitmapFile = SaveRestoreHelper.getProjectBitmapSaveFile(projectName, i);
            final int index = i;
            FileHelper.scanFile(bitmapFile, activity, new ScanFileCallback() {
                @Override
                public void onScanCompleted(Uri uri) {
                    try {
                        Bitmap thumbnail = ImageLoadingHelper.getThumbnailBitmapFromUri(activity, uri);
                        frameItem.setThumbnail(thumbnail);
                        frameItem.setUri(uri);
                        if (executeCounter.incrementAndGet() == frameItems.size()) {
                            callbackHandler.onRestore(frameItems);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        callbackHandler.onException(new RestoreProjectException(activity, SaveRestoreHelper.imageFileNamePattern(index), e));
                    }
                }
            });
            i++;
        }

        return null;
    }

    public interface RestoreProjectAsyncCallbackHandler {
        void onRestore(ArrayList<FrameItem> restoredFrames);
        void onException(RestoreProjectException e);
    }

}
