/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Evaluation;

import com.github.matejko219.techniqueevaluator.Drawing.Point;

import java.util.Arrays;
import java.util.List;

/**
 * Created by DELL on 2017-09-06.
 */
public class EvaluationUtils {

    public static final String HEAD = "HEAD";
    public static final String LEFT_SHOULDER = "LEFT SHOULDER";
    public static final String LEFT_ELBOW = "LEFT ELBOW";
    public static final String LEFT_WRIST = "LEFT WRIST";
    public static final String RIGHT_SHOULDER = "RIGHT SHOULDER";
    public static final String RIGHT_ELBOW = "RIGHT ELBOW";
    public static final String RIGHT_WRIST = "RIGHT WRIST";
    public static final String LEFT_HIP = "LEFT HIP";
    public static final String LEFT_KNEE = "LEFT KNEE";
    public static final String LEFT_ANKLE = "LEFT ANKLE";
    public static final String LEFT_FOOT = "LEFT FOOT";
    public static final String RIGHT_HIP = "RIGHT HIP";
    public static final String RIGHT_KNEE = "RIGHT KNEE";
    public static final String RIGHT_ANKLE = "RIGHT ANKLE";
    public static final String RIGHT_FOOT = "RIGHT FOOT";

    public static final int CIRCLE_RADIUS = 25;
    public static final int CIRCLE_RADIUS_SMALL = 20;

    public static final List<String> skeletonPointsOrder = Arrays
            .asList(HEAD, LEFT_SHOULDER, LEFT_ELBOW,
                    LEFT_WRIST, RIGHT_SHOULDER, RIGHT_ELBOW,
                    RIGHT_WRIST, LEFT_HIP, LEFT_KNEE,
                    LEFT_ANKLE, LEFT_FOOT, RIGHT_HIP,
                    RIGHT_KNEE, RIGHT_ANKLE, RIGHT_FOOT);

    public static float angleBetween2Lines(Point a, Point fixed, Point b) {
        float angle1 = (float) Math.atan2(a.y - fixed.y, a.x - fixed.x);
        float angle2 = (float) Math.atan2(b.y - fixed.y, b.x - fixed.x);
        float calculatedAngle = (float) Math.toDegrees(angle1 - angle2);
        if (calculatedAngle < 0) calculatedAngle += 360;
        return calculatedAngle;
    }

    public static double distanceBetween2Points(Point first, Point second) {
        return Math.sqrt(Math.pow(first.x - second.x, 2) + Math.pow(first.y - second.y, 2));
    }

    public static int getMaxSkeletonNumberOfPoints() {
        return skeletonPointsOrder.size();
    }
}
