/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Drawing;

import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Protractor;
import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Ruler;
import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Skeleton.Skeleton;

import java.util.List;

/**
 * Created by DELL on 2017-12-28.
 */
public class DrawingViewState extends View.BaseSavedState {

    Skeleton skeleton;
    Drawable selectedBitmap;
    DrawingView.Tool selectedTool;
    Protractor currentProtractor;
    List<Protractor> protractors;
    Ruler currentRuler;
    List<Ruler> rulers;
    double rulerScale;
    float correctionRatio;

    public DrawingViewState(Parcel source) {
        super(source);
    }

    public DrawingViewState(Parcelable superState) {
        super(superState);
    }

    public static final Parcelable.Creator<DrawingViewState> CREATOR =
            new Parcelable.Creator<DrawingViewState>() {
                @Override
                public DrawingViewState createFromParcel(Parcel source) {
                    return new DrawingViewState(source);
                }

                @Override
                public DrawingViewState[] newArray(int size) {
                    return new DrawingViewState[size];
                }
            };
}
