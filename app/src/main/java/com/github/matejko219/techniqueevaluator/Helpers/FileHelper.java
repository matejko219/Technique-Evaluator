/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Helpers;

import android.Manifest;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Mateusz Tumidajewicz (matejko219) on 2018-03-20.
 */
public class FileHelper {

    public enum AppDirectory {
        FRAMES("Frames"),
        SAVED_IMAGES("Saved Images"),
        SAVED_PROJECTS("Saved Projects");

        String value;

        AppDirectory(final String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public static boolean saveBitmap(Bitmap bitmap, AppDirectory appDirectory, Activity activity, ScanFileCallback callback) {
        if (ImageLoadingHelper.isStoragePermissionGranted(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            String timeStamp = getTimeStamp();
            File bitmapFile = new File(getAppDirectory(appDirectory), String.format("%s.png", timeStamp));
            boolean result = saveBitmapToFile(bitmap, bitmapFile);
            scanFile(bitmapFile, activity, callback);
            return result;
        } else return false;
    }

    public static boolean saveBitmapToFile(Bitmap bmp, File file) {
        BufferedOutputStream bos = null;

        try {
            bos = new BufferedOutputStream(new FileOutputStream(file));
            bmp.compress(Bitmap.CompressFormat.PNG, 90, bos);
            return true;

        } catch (IOException e) {
            Log.e("VideoViewUtils", "failed to save frame", e);

        } finally {
            try {
                if (bos != null) bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    public static String getTimeStamp() {
        return new SimpleDateFormat("dd-MM-yy_HH:mm:ss.SSS", Locale.ENGLISH).format(new Date());
    }

    public static String getShortTimeStamp() {
        return new SimpleDateFormat("dd-MM-yy HH:mm:ss", Locale.ENGLISH).format(new Date());
    }

    public static boolean isFileNameValid(String fileName) {
        if (fileName.equals(".") ||
                fileName.equals("..") ||
                fileName.contains("/") ||
                fileName.contains("\0") ||
                fileName.trim().isEmpty())
            return false;

        return true;
    }

    public static File getAppDirectory(AppDirectory appDirectory) {
        String externalStoragePath = Environment.getExternalStorageDirectory().toString();
        File bitmapDirectory = new File(externalStoragePath + "/TechniqueEvaluator/" + appDirectory);
        bitmapDirectory.mkdirs();
        return bitmapDirectory;
    }

    public static void scanFile(final File file, final Context context, final ScanFileCallback callback) {
        final MediaScannerConnection.MediaScannerConnectionClient mediaScannerConnectionClient = new MediaScannerConnection.MediaScannerConnectionClient() {

            private MediaScannerConnection msc = null;

            {
                msc = new MediaScannerConnection(context, this);
                msc.connect();
            }

            @Override
            public void onMediaScannerConnected() {
                msc.scanFile(file.getAbsolutePath(), null);
            }

            @Override
            public void onScanCompleted(String path, Uri uri) {
                if (callback != null) callback.onScanCompleted(uri);
                msc.disconnect();
            }
        };
    }

    public static String getPath(final Context context, final Uri uri) {
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};
                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    private static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    private static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

}
