/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Helpers;

import android.net.Uri;

/**
 * Created by Mateusz Tumidajewicz (matejko219) on 2018-03-24.
 */
public interface ScanFileCallback {

    void onScanCompleted(Uri uri);

}
