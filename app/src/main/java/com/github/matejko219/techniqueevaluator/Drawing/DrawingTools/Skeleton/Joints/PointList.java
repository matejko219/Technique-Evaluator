/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Skeleton.Joints;

import com.github.matejko219.techniqueevaluator.Drawing.Point;

import java.util.List;

/**
 * Created by DELL on 2017-10-31.
 */
public interface PointList {
    List<Point> getPointList();
}
