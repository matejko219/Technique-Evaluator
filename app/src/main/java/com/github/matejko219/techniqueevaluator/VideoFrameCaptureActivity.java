/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.matejko219.techniqueevaluator.Helpers.DialogHelper;
import com.github.matejko219.techniqueevaluator.Helpers.FileHelper;
import com.github.matejko219.techniqueevaluator.Helpers.ScanFileCallback;

import net.protyposis.android.mediaplayer.MediaPlayer;
import net.protyposis.android.mediaplayer.UriSource;
import net.protyposis.android.spectaculum.InputSurfaceHolder;
import net.protyposis.android.spectaculum.MediaPlayerExtendedView;
import net.protyposis.android.spectaculum.SpectaculumView;

import java.io.IOException;
import java.util.ArrayList;

public class VideoFrameCaptureActivity extends Activity implements InputSurfaceHolder.Callback {

    private MediaPlayerExtendedView videoView;

    private MediaController mediaController;
    private MediaPlayer mMediaPlayer;

    private Uri videoUri;
    private int videoPosition;
    private float videoPlaybackSpeed;
    private boolean videoPlaying;

    private TextView speedTextView;

    private ArrayList<String> frameUriStrings;

    private static final String SAVED_URI = "SAVED_URI";
    private static final String SAVED_POSITION = "SAVED_POSITION";
    private static final String SAVED_PLAYBACK_SPEED = "SAVED_PLAYBACK_SPEED";
    private static final String SAVED_PLAYING = "SAVED_PLAYING";
    private static final String SAVED_FRAMES_URIS = "SAVED_FRAMES_URIS";
    public static final String ACT_RESULT_DATA = "VIDEO_RESULT";

    private final int FRAME_M_SEC = 1000 / 24;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_frame_capture);

        frameUriStrings = new ArrayList<>();

        videoView = (MediaPlayerExtendedView) findViewById(R.id.video_view);
        videoView.getInputHolder().addCallback(this);
        videoView.setTouchEnabled(true);

        MediaController.MediaPlayerControl mediaPlayerControl = new MediaPlayerControl();
        mediaController = new MediaController(this);
        mediaController.setAnchorView(findViewById(R.id.video_view_container));
        mediaController.setMediaPlayer(mediaPlayerControl);
        mediaController.setEnabled(false);
        setPrevNextFrameListeners();

        String uriExtra = getIntent().getStringExtra(ImageSelectActivity.EXTRA_VIDEO_URI);
        videoUri = Uri.parse(uriExtra);
        videoPosition = 0;
        videoPlaybackSpeed = 1f;
        videoPlaying = false;
    }

    @Override
    public void onBackPressed() {
        Bundle resultBundle = new Bundle();
        resultBundle.putStringArrayList(ACT_RESULT_DATA, frameUriStrings);
        Intent resultIntent = new Intent();
        resultIntent.putExtras(resultBundle);
        setResult(RESULT_OK, resultIntent);
        super.onBackPressed();
    }

    @Override
    public void surfaceCreated(InputSurfaceHolder holder) {
        try {
            initPlayer();
        } catch (IOException e) {
            Log.e("SurfaceCreated", "error initializing player", e);
        }
    }

    @Override
    public void surfaceDestroyed(InputSurfaceHolder holder) {
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
        }
    }

    private void setPrevNextFrameListeners() {
        mediaController.setPrevNextListeners(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentMSec = mMediaPlayer.getCurrentPosition();
                mMediaPlayer.seekTo(currentMSec + FRAME_M_SEC);
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentMSec = mMediaPlayer.getCurrentPosition();
                mMediaPlayer.seekTo(currentMSec - FRAME_M_SEC);
            }
        });
    }

    private void initTextView() {
        speedTextView = (TextView) findViewById(R.id.playback_speed_text_view);
        setTextViewSpeedValue(videoPlaybackSpeed);
    }

    private void setTextViewSpeedValue(float speed) {
        speedTextView.setText(String.format("%.1f", speed));
    }

    private void initSeekBar() {
        SeekBar playbackSpeedSeekBar = (SeekBar) findViewById(R.id.playback_speed_seek_bar);
        playbackSpeedSeekBar.setMax(10);
        playbackSpeedSeekBar.incrementProgressBy(1);
        playbackSpeedSeekBar.setProgress((int) (videoPlaybackSpeed * 10));

        playbackSpeedSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float playbackSpeed = progress / 10f;
                setVideoPlaybackSpeed(playbackSpeed);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void setVideoPlaybackSpeed(float speed) {
        if (mMediaPlayer != null) {
            videoPlaybackSpeed = speed;
            mMediaPlayer.setPlaybackSpeed(videoPlaybackSpeed);

            if (videoPlaybackSpeed == 0f) mMediaPlayer.pause();
            else if (!mMediaPlayer.isPlaying()) mMediaPlayer.start();

            setTextViewSpeedValue(videoPlaybackSpeed);
        }
    }

    private void initPlayer() throws IOException {
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setSurface(videoView.getInputHolder().getSurface());

        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer vp) {
                mMediaPlayer.seekTo(videoPosition > 0 ? videoPosition : 0);
                mMediaPlayer.setPlaybackSpeed(videoPlaybackSpeed);

                if (videoPlaying) {
                    mMediaPlayer.start();
                }

                mediaController.setEnabled(true);
            }
        });

        mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                DialogHelper.showToast(VideoFrameCaptureActivity.this, getString(R.string.video_play_error));
                mediaController.setEnabled(false);
                return true;
            }
        });

        mMediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
            @Override
            public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                videoView.updateResolution(width, height);
                videoView.requestLayout();
            }
        });

        mMediaPlayer.setDataSource(new UriSource(VideoFrameCaptureActivity.this, videoUri));
        mMediaPlayer.prepareAsync();

        videoView.setOnFrameCapturedCallback(new SpectaculumView.OnFrameCapturedCallback() {
            @Override
            public void onFrameCaptured(Bitmap bitmap) {
                showCapturedFrame(bitmap);
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (mediaController.isShowing()) {
                mediaController.hide();
            } else {
                mediaController.show();
            }
        }
        return super.onTouchEvent(event);
    }

    @Override
    protected void onResume() {
        super.onResume();
        videoView.onResume();

        initTextView();
        initSeekBar();
    }

    @Override
    protected void onPause() {
        videoPosition = mMediaPlayer.getCurrentPosition();
        videoPlaybackSpeed = mMediaPlayer.getPlaybackSpeed();
        videoPlaying = mMediaPlayer.isPlaying();

        super.onPause();

        mMediaPlayer.release();
        mMediaPlayer = null;

        videoView.onPause();
    }

    @Override
    protected void onStop() {
        if (mediaController != null) mediaController.hide();
        super.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(SAVED_URI, videoUri);
        outState.putInt(SAVED_POSITION, videoPosition);
        outState.putFloat(SAVED_PLAYBACK_SPEED, videoPlaybackSpeed);
        outState.putBoolean(SAVED_PLAYING, videoPlaying);
        outState.putStringArrayList(SAVED_FRAMES_URIS, frameUriStrings);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        videoUri = savedInstanceState.getParcelable(SAVED_URI);
        videoPosition = savedInstanceState.getInt(SAVED_POSITION);
        videoPlaybackSpeed = savedInstanceState.getFloat(SAVED_PLAYBACK_SPEED);
        videoPlaying = savedInstanceState.getBoolean(SAVED_PLAYING);
        ArrayList<String> uriStrings = savedInstanceState.getStringArrayList(SAVED_FRAMES_URIS);
        if (uriStrings != null) frameUriStrings.addAll(uriStrings);
    }

    public void onCaptureFrameClick(View view) {
        mMediaPlayer.pause();
        videoView.captureFrame();
    }

    private void showCapturedFrame(final Bitmap frame) {
        if (frame == null) {
            DialogHelper.showToast(VideoFrameCaptureActivity.this, getString(R.string.frame_capture_error));

        } else {
            AlertDialog.Builder myCaptureDialog =
                    new AlertDialog.Builder(VideoFrameCaptureActivity.this);
            ImageView capturedImageView = new ImageView(VideoFrameCaptureActivity.this);
            capturedImageView.setImageBitmap(frame);
            LinearLayout.LayoutParams capturedImageViewLayoutParams =
                    new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
            capturedImageView.setLayoutParams(capturedImageViewLayoutParams);

            myCaptureDialog.setView(capturedImageView);
            myCaptureDialog.setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (FileHelper.saveBitmap(frame,
                            FileHelper.AppDirectory.FRAMES,
                            VideoFrameCaptureActivity.this, new ScanFileCallback() {
                                @Override
                                public void onScanCompleted(Uri uri) {
                                    frameUriStrings.add(uri.toString());
                                }
                            }))
                        DialogHelper.showToast(VideoFrameCaptureActivity.this, getString(R.string.frame_saved));
                    else
                        DialogHelper.showToast(VideoFrameCaptureActivity.this, getString(R.string.frame_save_error));
                }
            });
            myCaptureDialog.setNegativeButton(getString(R.string.cancel), null);
            myCaptureDialog.show();
        }
    }

    private class MediaPlayerControl implements MediaController.MediaPlayerControl {
        @Override
        public void start() {
            if (mMediaPlayer != null) mMediaPlayer.start();
        }

        @Override
        public void pause() {
            if (mMediaPlayer != null) mMediaPlayer.pause();
        }

        @Override
        public int getDuration() {
            return mMediaPlayer != null ? mMediaPlayer.getDuration() : 0;
        }

        @Override
        public int getCurrentPosition() {
            return mMediaPlayer != null ? mMediaPlayer.getCurrentPosition() : 0;
        }

        @Override
        public void seekTo(int pos) {
            if (mMediaPlayer != null) mMediaPlayer.seekTo(pos);
        }

        @Override
        public boolean isPlaying() {
            return mMediaPlayer != null && mMediaPlayer.isPlaying();
        }

        @Override
        public int getBufferPercentage() {
            return 0;
        }

        @Override
        public boolean canPause() {
            return true;
        }

        @Override
        public boolean canSeekBackward() {
            return true;
        }

        @Override
        public boolean canSeekForward() {
            return true;
        }

        @Override
        public int getAudioSessionId() {
            return mMediaPlayer != null ? mMediaPlayer.getAudioSessionId() : 0;
        }
    }
}
