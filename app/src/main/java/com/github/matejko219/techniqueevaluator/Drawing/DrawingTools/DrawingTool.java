/*
 * Copyright Mateusz Tumidajewicz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be found in the LICENSE file at https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE
 */

package com.github.matejko219.techniqueevaluator.Drawing.DrawingTools;

import android.os.Parcel;
import android.os.Parcelable;

import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Exceptions.PointNotIncludedException;
import com.github.matejko219.techniqueevaluator.Drawing.DrawingTools.Exceptions.TooManyPointsException;
import com.github.matejko219.techniqueevaluator.Drawing.DrawingView;
import com.github.matejko219.techniqueevaluator.Drawing.Point;
import com.github.matejko219.techniqueevaluator.Evaluation.EvaluationUtils;

import java.io.Serializable;
import java.util.List;

/**
 * Created by DELL on 2018-02-26.
 */
public abstract class DrawingTool implements Parcelable, Serializable {
    protected int numberOfPoints;
    final protected int maxNumberOfPoints;
    final protected DrawingView.Tool tool;

    public DrawingTool(int maxNumberOfPoints, DrawingView.Tool tool) {
        this.maxNumberOfPoints = maxNumberOfPoints;
        this.tool = tool;
    }

    protected DrawingTool(Parcel in, int maxNumberOfPoints, DrawingView.Tool tool) {
        numberOfPoints = in.readInt();
        this.maxNumberOfPoints = maxNumberOfPoints;
        this.tool = tool;
    }

    public abstract List<Point> getAllEditablePoints();

    public abstract void addPoint(Point point) throws TooManyPointsException;

    public Point getPointToEdit(Point pointToCheck) throws PointNotIncludedException {
        final int TOUCH_TOLERANCE = EvaluationUtils.CIRCLE_RADIUS * 2;

        for (Point point : getAllEditablePoints()) {
            float dx = Math.abs(point.x - pointToCheck.x);
            float dy = Math.abs(point.y - pointToCheck.y);

            if (dx <= TOUCH_TOLERANCE && dy <= TOUCH_TOLERANCE) {
                return point;
            }
        }

        throw new PointNotIncludedException(tool, pointToCheck);
    }

    public boolean containsPoint(Point pointToCheck) {
        for (Point point : getAllEditablePoints()) {
            if (point == pointToCheck) return true;
        }

        return false;
    }

    public int getNumberOfPoints() {
        return numberOfPoints;
    }

    public boolean isComplete() {
        return getNumberOfPoints() == maxNumberOfPoints;
    }

    public boolean canAddAnotherPoint() {
        return getNumberOfPoints() < maxNumberOfPoints;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(numberOfPoints);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
