TechniqueEvaluator
==================
A mobile application for the analysis of technical elements in sports training.

![app](screens/01.png?raw=true)

Download
========
To download go to [release](https://github.com/matejko219/Technique-Evaluator/releases) section.

License
=======
Copyright (C) 2017-2018 [Mateusz Tumidajewicz](mailto:tumidajewicz.mateusz@gmail.com). Released under the MIT license. See [LICENSE](https://github.com/matejko219/Technique-Evaluator/blob/master/LICENSE) for details.